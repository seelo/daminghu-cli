


## 操作步骤


1. 新建一个文件夹或者目录(比如dmhtest)
    ``` bash
    $ mkdir dmhtest
    $ cd dmhtest
    ```
2. 下载文件，移动文件到新建的目录(dmhtest)下
    - [windows](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-win.exe)
    - [linux](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-linux)
    - [mac](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-mac)


3. 在刚才新建的目录(dmhtest)下创建cfg.json文件
     ``` bash
        $ touch cfg.json
        $ ls
          cfg.json
          daminghu-cli-mac
     ```    
4. 编辑cfg.json

    `根据数据信息，修改 DBSchema,DBUsername,DBPassword,DBPort,DBIp`
    ```json
    {
         "PrefixProjectUri": "gitee.com/godY",
         "ProjectName": "test",
         "Port": "19527",
         "DistPath": "./dist/",
         "DBSchema": "mysql",
         "DBUsername": "root",
         "DBPassword": "root",
         "DBPort": "3306",
         "DBIp": "localhost",
         "DBConnParam": "?charset=utf8&parseTime=true&loc=Local&timeout=10s"
    }
    ```  
    
    | 名称  |  值   |
    |---|---|
    |  PrefixProjectUri |go项目的包名|
    |  ProjectName |项目名称|
    |  Port |服务端项目端口号|
    |  DistPath |项目生成目录|
    |  DBSchema |数据库-名称|
    |  DBUsername |数据库-用户名|
    |  DBPassword |数据库-密码|
    |  DBPort |数据库-端口号|
    |  DBIp |数据库-ip地址|
    |  DBConnParam |数据库-连接参数|


4. 运行命令，生成项目

>在刚才新建的目录(dmhtest)下执行命令
- windows
    - 打开cmd
    - `同步方式`  生成编译好的项目(可直接运行),`需要等待几分钟时间，不要关闭进程(受限网络环境,如果报错，超时，可选择邮件方式)`
        ```bash 
        $ daminghu-cli-win.exe -mode demo
        ```
    - ` 邮箱方式` 生成编译好的项目(可直接运行),发送到邮箱
   
        ```bash 
        $ daminghu-cli-win.exe -mode demo -email yasenagat@163.com
        ```
- linux
    - `同步方式`  生成编译好的项目(可直接运行),`需要等待几分钟时间，不要关闭进程(受限网络环境,如果报错，超时，可选择邮件方式)`
        ```bash 
        $ ./daminghu-cli-linux -mode demo
        ```
    - ` 邮箱方式` 生成编译好的项目(可直接运行),发送到邮箱
    
        ```bash 
        $ ./daminghu-cli-linux -mode demo -email yasenagat@163.com
        ```
- mac
    - `同步方式`  生成编译好的项目(可直接运行),`需要等待几分钟时间，不要关闭进程(受限网络环境,如果报错，超时，可选择邮件方式)`
        ```bash 
        $ ./daminghu-cli-mac -mode demo
        ```
    - ` 邮箱方式` 生成编译好的项目(可直接运行),发送到邮箱
    
        ```bash 
        $ ./daminghu-cli-mac -mode demo -email yasenagat@163.com
        ```
        
6. 启动程序

   - 同步方式
   
     目录结构如下:
        ```bash
           $ ls 
           drwxr-xr-x  5 nagatyase  staff   160B  3 18 15:43 conf
           drwxr-xr-x  4 nagatyase  staff   128B  3 18 15:43 dist
           -rwxr-xr-x  1 nagatyase  staff   6.0M  3 18 15:43 easyweb-linux
           -rwxr-xr-x  1 nagatyase  staff   6.0M  3 18 15:43 easyweb-mac
           -rwxr-xr-x  1 nagatyase  staff   5.9M  3 18 15:43 easyweb-win.exe
           drwxr-xr-x  3 nagatyase  staff    96B  3 18 15:43 sql
           -rwxr-xr-x  1 nagatyase  staff    18M  3 18 15:43 test-20190318154107-svr-linux
           -rwxr-xr-x  1 nagatyase  staff    18M  3 18 15:43 test-20190318154107-svr-mac
           -rwxr-xr-x  1 nagatyase  staff    18M  3 18 15:43 test-20190318154107-svr-win.exe
           
           $ tree
           .
           ├── conf
           │   ├── app.conf
           │   ├── db.conf
           │   └── upload.conf
           ├── dist
           │   ├── index.html
           │   └── static
           │       ├── css
           │       │   ├── app.70bef882a9b489582ee54a0e19cbffbc.css
           │       │   └── app.70bef882a9b489582ee54a0e19cbffbc.css.map
           │       ├── fonts
           │       │   └── element-icons.6f0a763.ttf
           │       └── js
           │           ├── app.b941b052ba407803605a.js
           │           ├── app.b941b052ba407803605a.js.map
           │           ├── manifest.2ae2e69a05c33dfc65f8.js
           │           ├── manifest.2ae2e69a05c33dfc65f8.js.map
           │           ├── vendor.1fe9ac8dde6ddefc0daf.js
           │           └── vendor.1fe9ac8dde6ddefc0daf.js.map
           ├── easyweb-linux
           ├── easyweb-mac
           ├── easyweb-win.exe
           ├── sql
           │   └── mysql
           │       ├── tb_user.base.xsql
           │       ├── tb_user.count.stpl
           │       ├── tb_user.read.stpl
           ├── test-20190318154107-svr-linux
           ├── test-20190318154107-svr-mac
           └── test-20190318154107-svr-win.exe
           
        ```
    1. windows
        
            双击 easyweb-win.exe
            双击 test-20190318154107-svr-win.exe
            
    1. linux
            
                ./easyweb-linux
                ./test-20190318154107-svr-linu
                
    1. mac
            
                ./easyweb-mac
                ./test-20190318154107-svr-mca
                
    - 邮件方式
   
       1. 下载附件
       2. 解压附件
       3. **按照同步方式，组织文件目录结构** `仔细看上面的目录结构,仔细看上面的目录结构,仔细看上面的目录结构,仔细看上面的目录结构,仔细看上面的目录结构,仔细看上面的目录结构,仔细看上面的目录结构`
       4. 按照同步方式，运行程序
                
                
                    
                    

   
     
        

   
    
    
   
    
    