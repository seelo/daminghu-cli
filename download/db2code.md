


## 操作步骤


1. 新建一个文件夹或者目录(比如dmhtest)
    ``` bash
    $ mkdir dmhtest
    $ cd dmhtest
    ```
2. 下载文件，移动文件到新建的目录(dmhtest)下
    - [windows](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-win.exe)
    - [linux](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-linux)
    - [mac](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-mac)


3. 在刚才新建的目录(dmhtest)下创建cfg.json文件
     ``` bash
        $ touch cfg.json
        $ ls
          cfg.json
          daminghu-cli-mac
     ```    
4. 编辑cfg.json

    `根据数据信息，修改 DBSchema,DBUsername,DBPassword,DBPort,DBIp`
    ```json
    {
         "PrefixProjectUri": "gitee.com/godY",
         "ProjectName": "test",
         "Port": "19527",
         "DistPath": "./dist/",
         "DBSchema": "mysql",
         "DBUsername": "root",
         "DBPassword": "root",
         "DBPort": "3306",
         "DBIp": "localhost",
         "DBConnParam": "?charset=utf8&parseTime=true&loc=Local&timeout=10s"
    }
    ```  
    
    | 名称  |  值   |
    |---|---|
    |  PrefixProjectUri |go项目的包名|
    |  ProjectName |项目名称|
    |  Port |服务端项目端口号|
    |  DistPath |项目生成目录|
    |  DBSchema |数据库-名称|
    |  DBUsername |数据库-用户名|
    |  DBPassword |数据库-密码|
    |  DBPort |数据库-端口号|
    |  DBIp |数据库-ip地址|
    |  DBConnParam |数据库-连接参数|


4. 运行命令，生成项目

>在刚才新建的目录(dmhtest)下执行命令
- windows
    - 打开cmd
    - `运行`
        ```bash 
        $ daminghu-cli-win.exe
- linux
    - `运行`
        ```bash 
        $ ./daminghu-cli-linux
        ```
- mac
    - `运行`
        ```bash 
        $ ./daminghu-cli-mac
        ```
        
6. 生成的项目

    生成一个后台项目test-svr
    
    生成一个前台项目test-web
    
    结构如下:
    ```bash
    $ tree -L 2
    .
    ├── test-svr
    │   ├── api
    │   ├── cmd
    │   ├── common
    │   ├── db
    │   ├── filter
    │   ├── model
    │   ├── msg
    │   └── router
    └── test-web
        ├── README.md
        ├── build
        ├── config
        ├── index.html
        ├── package.json
        ├── src
        ├── static
        └── test
    
    15 directories, 3 files
    
    ```
    
7. 用相关ide打开项目，进行开发

    - WebStome
    
    - GoLand

   
                
                
                    
                    

   
     
        

   
    
    
   
    
    