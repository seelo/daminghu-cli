# daminghu-cli(`项目`**源码**`生成`工具)

> 项目**源码**生成`工具`。

> 生成前后分离的两个项目**源码**，一个**【服务端】**项目，一个**【web】**项目。

## 特点

- **有源码 =! 项目可以运行起来！！！**使用此工具生成的项目，**`可以运行，可以运行，可以运行`**。

- 急速生成

- 不用写一行代码

- 生成的两个项目源码，编译后都可以独立部署

- 可以单独生成`服务端`项目
    - 对应web端的所有CRUD接口
- 可以单独生成`web`项目
    - 登录
    - 左侧导航菜单
    - headr面包屑，退出
    - CRUD所有操作
    - 分页查询
    - 首页
    - 图表统计
    - token，登录超时

  [在线查看-1](http://www.mlsdsj.com:29527/web)
  
  [在线查看-2](http://mlsdsj.com:13713/)

  [在线查看-3](http://www.mlsdsj.com:9977/)
  
  
- 一键启动查看效果( [点击查看效果](https://gitee.com/godY/daminghu-cli/blob/master/download/pic/demo.md) )

> ### 只有一个数据库，想查看此软件运行效果， [点击查看操作步骤](https://gitee.com/godY/daminghu-cli/blob/master/download/db2demo.md)

> ### 只有一个数据库，想直接生成项目源码， [点击查看操作步骤](https://gitee.com/godY/daminghu-cli/blob/master/download/db2code.md)

> ### 无数据库，啥也没有就是任性，[点击查看效果](https://gitee.com/godY/daminghu-cli/blob/master/download/pic/demo.md)

> ### 大神，技术人员，请继续往下看~

## 准备工作

- 安装 go (编译生成的sever项目源码)
    
    ```bash 
    $ go version
    go version go1.9.2 darwin/amd64
    ```
- 安装 npm (编译生成的web项目源码)

    ```bash 
    $ npm -version 
    6.7.0
    ```
- 安装 mysql 数据库(或者有一个本地`可以访问`的mysql数据库)

## 安装

> 项目安装提供两种方式,**实用党**可选择直接下载，**理论党**可以选择源码安装

- 直接下载(已经编译好的可执行文件)
    - [windows](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-win.exe)
    - [linux](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-linux)
    - [mac](https://gitee.com/godY/daminghu-cli/raw/master/download/daminghu-cli-mac)
- 源码安装
    ``` bash
    # install
    go install gitee.com/godY/daminghu-cli
    ```
    > ~~最好把gopath下的bin目录加入到系统的path里~~

## 使用方法

1. 新建一个文件夹或者目录(名称随意)
    ``` bash
    $ mkdir dmhtest
    $ cd dmhtest
    ```
2. 在刚才新建的目录下创建cfg.json文件
     ``` bash
        $ touch cfg.json
        $ ls
          cfg.json
          daminghu-cli-mac
     ```    
3. 编辑cfg.json

    ```json
    {
         "PrefixProjectUri": "gitee.com/godY",
         "ProjectName": "xxxx",
         "Port": "19527",
         "DistPath": "./dist/",
         "DBSchema": "mysql",
         "DBUsername": "root",
         "DBPassword": "root",
         "DBPort": "3306",
         "DBIp": "localhost",
         "DBConnParam": "?charset=utf8&parseTime=true&loc=Local&timeout=10s"
    }
    ```  
    
    | 名称  |  值   |
    |---|---|
    |  PrefixProjectUri |go项目的包名|
    |  ProjectName |项目名称|
    |  Port |服务端项目端口号|
    |  DistPath |项目生成目录|
    |  DBSchema |数据库-名称|
    |  DBUsername |数据库-用户名|
    |  DBPassword |数据库-密码|
    |  DBPort |数据库-端口号|
    |  DBIp |数据库-ip地址|
    |  DBConnParam |数据库-连接参数|

4. 运行命令，生成项目

    根据安装方式不同，分为两种情况

    > 下载-方式
        
   1. 把下载的**文件**，放到**dmhtest**文件夹中
   2. 运行命令
      1. windows -> 双击 `daminghu-cli-win.exe`
      2. mac -> `./daminghu-cli-mac`
      2. linux -> `./daminghu-cli-linux`

    > 源码安装-方式
    1. 如果gopath的bin在系统的path目录下,直接在**dmhtest**
    **目录下**运行`daminghu-cli`命令
    2. 如果gopath的bin不在系统的path目录下,找到gopath/bin下面的daminghu-cli文件,放到**dmhtest**文件夹中，参考**上面几行**下载-方式，运行命令
    
5. 执行结果
``` bash
$ daminghu-cli
   2019/03/14 14:36:39 [D] MySqlConnStr  root:root@tcp(localhost:3306)/mysql?charset=utf8&parseTime=true&loc=Local&timeout=10s
   2019/03/14 14:36:39 [I] Start Before Handler..............
   2019/03/14 14:36:39 [I] Invoke Before Handler Complete!
   2019/03/14 14:36:39 [I] Start Build Svr Project..............
   2019/03/14 14:36:39 [D] CreateDirHandler
   2019/03/14 14:36:39 [D] DownloadTemplateHandler
   2019/03/14 14:36:40 [D] CreateBaseProjectHandler
   2019/03/14 14:36:40 [D] CreateRouterHandler
   2019/03/14 14:36:40 [D] CreateStructHandler
   2019/03/14 14:36:40 [D] CreateControllerHandler
   2019/03/14 14:36:40 [D] CreateSqlHandler
   2019/03/14 14:36:40 [D] CreateModelHandler
   2019/03/14 14:36:40 [I] Build Svr Project Complete!
   2019/03/14 14:36:40 [I] Start Build Web Project..............
   2019/03/14 14:36:40 [D] CreateDirHandler
   2019/03/14 14:36:40 [D] DownloadHandler
   2019/03/14 14:36:41 [D] DownloadTemplateHandler
   2019/03/14 14:36:41 [D] CreateApiHandler
   2019/03/14 14:36:41 [D] CreateEnvHandler
   2019/03/14 14:36:41 [D] CreateRouterHandler
   2019/03/14 14:36:41 [D] CreateMainHandler
   2019/03/14 14:36:41 [D] CreateHomeHandler
   2019/03/14 14:36:41 [D] CreatePageHandler
   2019/03/14 14:36:41 [I] Build Web Project Complete!
   2019/03/14 14:36:41 [I] Start After Handler..............
   2019/03/14 14:36:41 [D] DeleteTempHandler
   2019/03/14 14:36:41 [I] Invoke After Handler Complete!
```


## 生成的项目目录


``` bash
$ tree -L 2
.
├── cfg.json
└── dist
    ├── xxxx-svr //server项目
    └── xxxx-web //web项目

3 directories, 1 file
```

## 运行项目

-  服务端项目

  >copy项目到对应的gopath下
  
   举例子
   
   1. gopath = /Users/nagatyase/goproject
   2. cfg.json中的PrefixProjectUri = gitee.com/godY
   3. 把生成的xxxx-svr项目copy到/Users/nagatyase/goproject/src/gitee.com/godY下
   4. 最终位置 **/Users/nagatyase/goproject**/`src`/**gitee.com/godY**/`xxxx-svr`
   5. 进入到xxxx-svr/**cmd**文件夹,运行下面命令
```bash
~/src/gitee.com/godY/xxxx-svr/cmd  $ go run main.go
2019/03/14 15:01:19 [D] [db.go:16] db init
[xorm] [info]  2019/03/14 15:01:19.256224 PING DATABASE mysql
2019/03/14 15:01:19 [I] [asm_amd64.s:2337] http rpcserver Running on http://:19527
```

-  web项目

进入到dist/xxxx-web目录内，运行如下命令
```bash
~/dist/xxxx-web  $ npm install && npm run dev

 DONE  Compiled successfully in 20585ms                                                                                                                           下午2:59:05

 I  Your application is running here: http://localhost:19527
```
自动打开浏览器，开始一波操作吧。

## server目录

```bash
$ tree
.
├── api
│   ├── api_columns_priv.go
│   ├── api_db.go
│   ├── api_event.go
│   ├── api_func.go
│   ├── api_help_category.go
│   ├── api_help_keyword.go
│   ├── api_help_relation.go
│   ├── api_help_topic.go
│   ├── api_innodb_index_stats.go
│   ├── api_innodb_table_stats.go
│   ├── api_ndb_binlog_index.go
│   ├── api_plugin.go
│   ├── api_proc.go
│   ├── api_procs_priv.go
│   ├── api_proxies_priv.go
│   ├── api_servers.go
│   ├── api_slave_master_info.go
│   ├── api_slave_relay_log_info.go
│   ├── api_slave_worker_info.go
│   ├── api_tables_priv.go
│   ├── api_time_zone.go
│   ├── api_time_zone_leap_second.go
│   ├── api_time_zone_name.go
│   ├── api_time_zone_transition.go
│   ├── api_time_zone_transition_type.go
│   ├── api_user.go
│   ├── base.go
│   └── login.go
├── cmd
│   ├── conf
│   │   ├── app.conf
│   │   ├── db.conf
│   │   └── upload.conf
│   ├── logs
│   ├── main.go
│   └── sql
│       └── mysql
│           ├── columns_priv.base.xsql
│           ├── columns_priv.count.stpl
│           ├── columns_priv.read.stpl
│           ├── db.base.xsql
│           ├── db.count.stpl
│           ├── db.read.stpl
│           ├── event.base.xsql
│           ├── event.count.stpl
│           ├── event.read.stpl
│           ├── func.base.xsql
│           ├── func.count.stpl
│           ├── func.read.stpl
│           ├── help_category.base.xsql
│           ├── help_category.count.stpl
│           ├── help_category.read.stpl
│           ├── help_keyword.base.xsql
│           ├── help_keyword.count.stpl
│           ├── help_keyword.read.stpl
│           ├── help_relation.base.xsql
│           ├── help_relation.count.stpl
│           ├── help_relation.read.stpl
│           ├── help_topic.base.xsql
│           ├── help_topic.count.stpl
│           ├── help_topic.read.stpl
│           ├── innodb_index_stats.base.xsql
│           ├── innodb_index_stats.count.stpl
│           ├── innodb_index_stats.read.stpl
│           ├── innodb_table_stats.base.xsql
│           ├── innodb_table_stats.count.stpl
│           ├── innodb_table_stats.read.stpl
│           ├── ndb_binlog_index.base.xsql
│           ├── ndb_binlog_index.count.stpl
│           ├── ndb_binlog_index.read.stpl
│           ├── plugin.base.xsql
│           ├── plugin.count.stpl
│           ├── plugin.read.stpl
│           ├── proc.base.xsql
│           ├── proc.count.stpl
│           ├── proc.read.stpl
│           ├── procs_priv.base.xsql
│           ├── procs_priv.count.stpl
│           ├── procs_priv.read.stpl
│           ├── proxies_priv.base.xsql
│           ├── proxies_priv.count.stpl
│           ├── proxies_priv.read.stpl
│           ├── servers.base.xsql
│           ├── servers.count.stpl
│           ├── servers.read.stpl
│           ├── slave_master_info.base.xsql
│           ├── slave_master_info.count.stpl
│           ├── slave_master_info.read.stpl
│           ├── slave_relay_log_info.base.xsql
│           ├── slave_relay_log_info.count.stpl
│           ├── slave_relay_log_info.read.stpl
│           ├── slave_worker_info.base.xsql
│           ├── slave_worker_info.count.stpl
│           ├── slave_worker_info.read.stpl
│           ├── tables_priv.base.xsql
│           ├── tables_priv.count.stpl
│           ├── tables_priv.read.stpl
│           ├── time_zone.base.xsql
│           ├── time_zone.count.stpl
│           ├── time_zone.read.stpl
│           ├── time_zone_leap_second.base.xsql
│           ├── time_zone_leap_second.count.stpl
│           ├── time_zone_leap_second.read.stpl
│           ├── time_zone_name.base.xsql
│           ├── time_zone_name.count.stpl
│           ├── time_zone_name.read.stpl
│           ├── time_zone_transition.base.xsql
│           ├── time_zone_transition.count.stpl
│           ├── time_zone_transition.read.stpl
│           ├── time_zone_transition_type.base.xsql
│           ├── time_zone_transition_type.count.stpl
│           ├── time_zone_transition_type.read.stpl
│           ├── user.base.xsql
│           ├── user.count.stpl
│           └── user.read.stpl
├── common
│   └── common.go
├── db
│   ├── db.go
│   ├── db_columns_priv.go
│   ├── db_db.go
│   ├── db_event.go
│   ├── db_func.go
│   ├── db_help_category.go
│   ├── db_help_keyword.go
│   ├── db_help_relation.go
│   ├── db_help_topic.go
│   ├── db_innodb_index_stats.go
│   ├── db_innodb_table_stats.go
│   ├── db_ndb_binlog_index.go
│   ├── db_plugin.go
│   ├── db_proc.go
│   ├── db_procs_priv.go
│   ├── db_proxies_priv.go
│   ├── db_servers.go
│   ├── db_slave_master_info.go
│   ├── db_slave_relay_log_info.go
│   ├── db_slave_worker_info.go
│   ├── db_tables_priv.go
│   ├── db_time_zone.go
│   ├── db_time_zone_leap_second.go
│   ├── db_time_zone_name.go
│   ├── db_time_zone_transition.go
│   ├── db_time_zone_transition_type.go
│   └── db_user.go
├── filter
│   └── filter.go
├── model
│   ├── base.go
│   ├── model_columns_priv.go
│   ├── model_db.go
│   ├── model_event.go
│   ├── model_func.go
│   ├── model_help_category.go
│   ├── model_help_keyword.go
│   ├── model_help_relation.go
│   ├── model_help_topic.go
│   ├── model_innodb_index_stats.go
│   ├── model_innodb_table_stats.go
│   ├── model_ndb_binlog_index.go
│   ├── model_plugin.go
│   ├── model_proc.go
│   ├── model_procs_priv.go
│   ├── model_proxies_priv.go
│   ├── model_servers.go
│   ├── model_slave_master_info.go
│   ├── model_slave_relay_log_info.go
│   ├── model_slave_worker_info.go
│   ├── model_tables_priv.go
│   ├── model_time_zone.go
│   ├── model_time_zone_leap_second.go
│   ├── model_time_zone_name.go
│   ├── model_time_zone_transition.go
│   ├── model_time_zone_transition_type.go
│   └── model_user.go
├── msg
│   └── base.go
└── router
    └── router.go

12 directories, 168 files
```

## web目录
```bash
$ tree
.
├── README.md
├── build
│   ├── build.js
│   ├── check-versions.js
│   ├── logo.png
│   ├── utils.js
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js
│   ├── webpack.dev.conf.js
│   └── webpack.prod.conf.js
├── config
│   ├── dev.env.js
│   ├── index.js
│   ├── prod.env.js
│   └── test.env.js
├── index.html
├── package.json
├── src
│   ├── App.vue
│   ├── api
│   │   ├── api.js
│   │   ├── http.js
│   │   └── util.js
│   ├── assets
│   │   └── logo.png
│   ├── components
│   │   └── headTop.vue
│   ├── config
│   │   └── env.js
│   ├── main.js
│   ├── mixin
│   ├── pages
│   │   ├── columns_priv.vue
│   │   ├── db.vue
│   │   ├── event.vue
│   │   ├── func.vue
│   │   ├── help_category.vue
│   │   ├── help_keyword.vue
│   │   ├── help_relation.vue
│   │   ├── help_topic.vue
│   │   ├── home.vue
│   │   ├── innodb_index_stats.vue
│   │   ├── innodb_table_stats.vue
│   │   ├── login.vue
│   │   ├── main.vue
│   │   ├── ndb_binlog_index.vue
│   │   ├── plugin.vue
│   │   ├── proc.vue
│   │   ├── procs_priv.vue
│   │   ├── proxies_priv.vue
│   │   ├── servers.vue
│   │   ├── slave_master_info.vue
│   │   ├── slave_relay_log_info.vue
│   │   ├── slave_worker_info.vue
│   │   ├── tables_priv.vue
│   │   ├── time_zone.vue
│   │   ├── time_zone_leap_second.vue
│   │   ├── time_zone_name.vue
│   │   ├── time_zone_transition.vue
│   │   ├── time_zone_transition_type.vue
│   │   └── user.vue
│   ├── router
│   │   └── index.js
│   └── style
│       ├── common.less
│       └── mixin.less
├── static
│   └── img
└── test
    ├── e2e
    │   ├── custom-assertions
    │   │   └── elementCount.js
    │   ├── nightwatch.conf.js
    │   ├── runner.js
    │   └── specs
    │       └── test.js
    └── unit
        ├── jest.conf.js
        ├── setup.js
        └── specs
            └── HelloWorld.spec.js

19 directories, 62 files
```

## 命令参数


- 生成两个项目 -> daminghu-cli -build all

- 生成server项目 -> daminghu-cli -build svr

- 生成web项目 -> daminghu-cli -build web

- 指定配置文件位置-> daminghu-cli -cfg ./cfg.json

- 生成编译好的项目(可直接运行),`需要等待几分钟时间，不要关闭进程` -> daminghu-cli -mode demo

- 生成编译好的项目(可直接运行),发送到邮箱-> daminghu-cli -mode demo -email yasenagat@163.com

