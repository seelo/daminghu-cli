package main

import (
	"flag"
	"gitee.com/godY/daminghu-cli/cmd"
	"gitee.com/godY/daminghu-cli/common"
)

func main() {

	buildType := flag.String("build", "all", "build type : all,svr,web")
	cfgPath := flag.String("cfg", "./cfg.json", "cfg path")
	mode := flag.String("mode", "dev", "dev,demo")
	jsondata := flag.String("jsondata", "", "json data file path")
	email := flag.String("email", "", "when mode=demo,the created project will send to the email")
	flag.Parse()

	common.CmdParam.BuildType = *buildType
	common.CmdParam.CfgPath = *cfgPath
	common.CmdParam.Mode = *mode
	common.CmdParam.Jsondata = *jsondata
	common.CmdParam.Email = *email

	cmds := []string{}
	cmds = append(cmds, "cfg")
	cmds = append(cmds, "mode")
	cmds = append(cmds, "jsondata")
	cmds = append(cmds, "email")
	cmds = append(cmds, "build")

	values := []string{}
	values = append(values, *cfgPath)
	values = append(values, *mode)
	values = append(values, *jsondata)
	values = append(values, *email)
	values = append(values, *buildType)
	if *buildType != "" && *cfgPath != "" && *mode != "" {
		//logs.Info(*buildType, *cfgPath)
		cmd.Start(cmds, values)
	} else {
		flag.Usage()
	}
}
