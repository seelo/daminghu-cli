package cfg

var (
	Cfg Config
)

type Config struct {
	PrefixProjectUri   string
	ProjectName        string
	Port               string
	PreUrl             string
	LocalTemplate      bool
	ServerTemplatePath string
	WebTemplatePath    string
	//ServerDistPath     string
	//WebDistPath        string
	DistPath string

	DBSchema    string
	DBUsername  string
	DBPassword  string
	DBPort      string
	DBIp        string
	DBConnParam string
}

func (cfg Config) GetMySqlConn() string {
	return cfg.DBUsername + ":" + cfg.DBPassword + "@tcp(" + cfg.DBIp + ":" + cfg.DBPort + ")/" + cfg.DBSchema + cfg.DBConnParam
}

func (cfg Config) GetFullProjectUri() string {
	return cfg.PrefixProjectUri + "/" + cfg.GetServerProjcetName()
}

func (cfg Config) GetServerProjcetPath() string {
	return cfg.DistPath + "" + cfg.GetServerProjcetName() + "/"
}

func (cfg Config) GetWebProjcetPath() string {
	return cfg.DistPath + "" + cfg.GetWebProjcetName() + "/"
}

func (cfg Config) GetWebProjcetSrcPath() string {
	return cfg.GetWebProjcetPath() + "src/"
}

func (cfg Config) GetServerProjcetName() string {
	return cfg.ProjectName + "-svr"
}

func (cfg Config) GetWebProjcetName() string {
	return cfg.ProjectName + "-web"
}

func (cfg Config) GetWebTempPath() string {
	return cfg.DistPath + "temp/"
}

func (cfg Config) GetServerTempPath() string {
	return cfg.DistPath + "temp/"
}
