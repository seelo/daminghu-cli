#!/bin/bash
dist="./download/"
main="main.go"
appname="daminghu-cli"

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ${dist}${appname}-linux ${main}

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ${dist}${appname} ${main}

CGO_ENABLED=1 GOOS=windows GOARCH=amd64 go build -o ${dist}${appname}-win.exe ${main}

CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o ${dist}${appname}-mac ${main}


