//{{.Auth}}
import http from "@/api/http"
import {Message} from 'element-ui';
export const api_login = data => http('/readlogin', data, 'POST');

export function isResultOk(response) {
    if (response) {
        if (response.result == 0) {
            Message.info({message: '操作成功'})
            return true
        } else {
            Message.error({message: '操作失败'})
            return false
        }
        // else if (response.result == -10) {
        //     router.push("/")
        // }
    } else {
        Message.error({message: '操作失败'})
        return false
    }

}

{{with .Tables}}

		    {{range $k, $v := .}}

            export const api_readmany_{{Low .Name}} = data => http('/{{ReplaceUnderLineLow .Name}}/readmany', data);
            export const api_readcount_{{Low .Name}} = data => http('/{{ReplaceUnderLineLow .Name}}/readcount', data);
            export const api_deleteone_{{Low .Name}} = data => http('/{{ReplaceUnderLineLow .Name}}/deleteone', data);
            export const api_updateone_{{Low .Name}} = data => http('/{{ReplaceUnderLineLow .Name}}/updateone', data, 'POST');
            export const api_updatexxx_{{Low .Name}} = data => http('/{{ReplaceUnderLineLow .Name}}/updatexxx', data, 'POST');
            export const api_createone_{{Low .Name}} = data => http('/{{ReplaceUnderLineLow .Name}}/createone', data, 'POST');
        	{{end}}
        {{end}}


