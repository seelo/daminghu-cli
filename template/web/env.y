var baseUrl = ""
var imgPreUrl = "https://host"
if (process.env.NODE_ENV == 'development') {
    baseUrl = 'http://localhost:{{.Port}}/api';
} else {
    baseUrl = 'http://localhost:{{.Port}}/api';
}

export {baseUrl, imgPreUrl}
