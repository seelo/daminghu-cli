<!--{{.Auth}}-->
<template>
    <div class="manage_page fillcontain">
        <el-row style="height: 100%;">
            <el-col :span="4"  style="min-height: 100%; background-color: #eef1f6;">
                <el-menu :default-active="defaultActive" style="min-height: 100%;" theme="dark" router>
                    <el-menu-item index="home"><i class="el-icon-menu"></i>首页</el-menu-item>
                    <el-submenu index="2">
                        <template slot="title"><i class="el-icon-document"></i>数据管理</template>
                        {{with .Tables}}
                                                		    {{range $k, $v := .}}
                                                <el-menu-item index="{{Low .Name}}">
                                                {{if .Comment}}
                                                                            {{Low .Comment}}
                                                                        {{else}}
                                                                            {{Low .Name}}
                                                                        {{end}}
                                                </el-menu-item>
                                                        	{{end}}
                                                        {{end}}
                    </el-submenu>
                    <!--<el-submenu index="3">-->
                        <!--<template slot="title"><i class="el-icon-plus"></i>x</template>-->
                        <!--<el-menu-item index="xx">xx</el-menu-item>-->
                        <!--<el-menu-item index="xxx">xxx</el-menu-item>-->
                    <!--</el-submenu>-->

                </el-menu>
            </el-col>
            <el-col :span="20" style="height: 100%;overflow: auto;">
                <headTop></headTop>
                <keep-alive>
                    <router-view></router-view>
                </keep-alive>
            </el-col>
        </el-row>
    </div>
</template>

<script>
import headTop from '@/components/headTop'
    export default {
    components: {
                 headTop,
    },
        computed: {
            defaultActive: function(){
                return this.$route.path.replace('/', '');
            }
        },
    }
</script>


<style lang="less" scoped>
    @import '../style/mixin';
    .manage_page{

    }
</style>
