<!--{{.Auth}}-->
{{$strCount := "count"}}
{{$leftQ := "{{"}}
{{$rightQ := "}}"}}
<template>
    <div class="fillcontain">

        <ve-pie :data="chartData"></ve-pie>

        <section class="data_section">
            <header class="data_header">数据统计</header>
            <el-row :gutter="12">

            {{with .Tables}}
                {{range $k, $v := .}}

            <el-col class="cel" :span="8">
                <el-card :body-style="{ padding: '10px' }" shadow="hover">
                    <div @click="handlerClick('/{{Low .Name}}')" class="data">
                        <div class="label">{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}数量</div>
                        <div class="count">{{$leftQ}} {{$strCount}}{{$k}} {{$rightQ}}</div>
                    </div>
                </el-card>
            </el-col>

                {{end}}
            {{end}}


            </el-row>
        </section>
    </div>
</template>

<script>

    import {

    {{with .Tables}}
        {{range $k, $v := .}}

            api_readcount_{{Low .Name}},

        {{end}}
    {{end}}

    } from "../api/api";

    export default {
        data() {
            return {
            {{with .Tables}}
                {{range $k, $v := .}}

                    count{{$k}}:0,

                {{end}}
            {{end}}
                form: {},
                option: {
                    data: []
                },
                chartData: {
                }
            }
        }
        ,
        components: {}
        ,
        created() {
            this.initData();
        }
        ,
        methods: {
            async initData() {

                Promise.all([
                {{with .Tables}}
                    {{range $k, $v := .}}

                        api_readcount_{{Low .Name}}(),

                    {{end}}
                {{end}}
                ]).then(res => {
                {{with .Tables}}
                    {{range $k, $v := .}}

                        this.count{{$k}} = res[{{$k}}].body.count;

                    {{end}}
                {{end}}
                    this.chartData = {
                        columns: ['label', 'count'],
                        rows: [

                        {{with .Tables}}
                            {{range $k, $v := .}}

                                {'label': '{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}数量', 'count': this.count{{$k}}},

                            {{end}}
                        {{end}}


                        ]
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
            ,
            handlerClick(path) {
                this.$router.push({path: path})
            }
        }
        ,
    }
</script>

<style lang="less">
    @import '../style/mixin';

    .data_section {
        padding: 20px;
        margin-bottom: 40px;
        .data_header {
            text-align: center;
            font-size: 30px;
            margin-bottom: 10px;
        }
        .cel {
            padding: 20px;
        }
        .data {
            margin: 10px;
            padding: 10px;
            /*padding: 20px;*/
            text-align: center;
            font-size: 14px;
            color: #666;
            border-radius: 6px;
            background: #E5E9F2;
            .count {
                color: #20A1FF;
                font-size: 26px;

            }
            .label {
                border-radius: 6px;
                font-size: 22px;
                padding: 4px 0;
                color: #FF8800;
                display: inline-block;
            }
        }
    }


</style>
