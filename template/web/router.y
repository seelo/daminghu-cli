//{{.Auth}}
import Vue from 'vue'
import Router from 'vue-router'
import login from '@/pages/login.vue'
import main from '@/pages/main.vue'
import home from '@/pages/home.vue'

{{$t := .Tables}}
{{with .Tables}}

		    {{range $k, $v := .}}
import {{Low .Name}} from '@/pages/{{Low .Name}}.vue'
        	{{end}}
        {{end}}

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'login',
            component: login
        },
        {
            path: '/main',
            name: 'main',
            component: main,
            children: [
            {
                    path: '/home',
                    name: 'home',
                    component: home,
                    meta: {
                        requiresAuth: true,
                        navpath: ['首页']
                    }
                },
            {{with .Tables}}

            		    {{range $k, $v := .}}
             {
                            path: '/{{Low .Name}}',
                            component: {{Low .Name}},
                            meta: {
                                requiresAuth: true,
                                navpath: ['数据管理', '{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}']
                            }
                        }{{IsLastTable $k $t}}
                    	{{end}}
                    {{end}}

            ]
        }
    ]
})
