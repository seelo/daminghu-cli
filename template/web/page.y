<!--{{.Auth}}-->
{{$queryOneFormData := "queryOneFormData"}}
{{$leftQ := "{{"}}
{{$rightQ := "}}"}}

<template>
    <div class="fillcontain">

        <div class="table_container">

            <el-form :inline="true" :model="queryForm" ref="queryForm" class="demo-form-inline">

            {{with .Columns}}
            	{{range $k, $v := .}}

            	<el-form-item label="{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}" prop="{{Low .Name}}">
                                    <el-input v-model="queryForm.{{Low .Name}}" placeholder="请输入{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}"></el-input>
                                </el-form-item>
            	{{end}}
            {{end}}
                <el-form-item>
                    <el-button type="primary" icon="el-icon-search" @click="handleQuery">查询</el-button>
                    <el-button type="primary" icon="el-icon-delete" @click="handleClear">清空</el-button>
                </el-form-item>
            </el-form>
            <el-form>
                <el-form-item>
                    <el-button type="primary" icon="el-icon-plus" @click="handleCreate">立即创建</el-button>
                </el-form-item>
            </el-form>

            <el-table
                ref="queryTable"
                border
                stripe
                :data="tableData"
                @row-click="handleRowClick"
                highlight-current-row
                style="width: 100%">
                <el-table-column type="expand">
                    <template slot-scope="props">
                        <el-form label-position="left" inline class="demo-table-expand">

                            {{with .Columns}}
                                        	{{range $k, $v := .}}

                             <el-form-item label-width="150px" label="{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}">
                                                            <span>{{ToPropName .Name}}</span>
                                                        </el-form-item>
                                        	{{end}}
                                        {{end}}
                        </el-form>
                    </template>
                </el-table-column>
                <el-table-column
                    type="index"
                    label="序号"
                    width="80">
                </el-table-column>
{{with .Columns}}
            	{{range $k, $v := .}}
                                <el-table-column
                                                    property="{{Low .Name}}"
                                                    label="{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}"
                                                    width="120">
                                                </el-table-column>
            	{{end}}
            {{end}}
                <el-table-column label="操作" width="200">
                    <template slot-scope="scope">
                        <!--<el-button
                            size="mini"
                            type="success" icon="el-icon-edit"
                            @click="handleQueryOne(scope.$index, scope.row)">查看
                        </el-button>-->
                        <el-button
                            size="mini"
                            type="success" icon="el-icon-edit"
                            @click="handleUpdate(scope.$index, scope.row)">编辑
                        </el-button>
                        <el-button
                            size="mini"
                            type="danger" icon="el-icon-close"
                            @click="handleDelete(scope.$index, scope.row)">删除
                        </el-button>
                    </template>
                </el-table-column>
            </el-table>
            <div class="Pagination" style="text-align: left;margin-top: 10px;">
                <el-pagination
                    @size-change="handleSizeChange"
                    @current-change="handleCurrentChange"
                    :current-page="currentPage"
                    :page-size="pageSize"
                    :page-sizes="pageSizes"
                    layout="total, sizes, prev, pager, next, jumper"
                    :total="count">
                </el-pagination>
            </div>

            <el-dialog :title="commandDialogTitle" :visible.sync="commandDialogFormVisible">
                <el-form :model="commandFormData" :rules="rules" ref="commandForm">

                {{with .Columns}}
                            	{{range $k, $v := .}}

                            	<el-form-item label="{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}" prop="{{Low .Name}}">
                                                    <el-input v-model="commandFormData.{{Low .Name}}" placeholder="请输入{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}"></el-input>
                                                </el-form-item>
                            	{{end}}
                            {{end}}
                </el-form>
                <div slot="footer" class="dialog-footer">
                    <el-button @click="commandDialogFormVisible = false">取 消</el-button>
                    <el-button type="primary" @click="commandUser('commandForm')">确 定</el-button>
                </div>
            </el-dialog>

            <el-dialog :title="queryOneDialogTitle" :visible.sync="queryOneDialogFormVisible">
                <el-form :model="queryOneFormData" class="view-dialog">

                {{with .Columns}}
                            	{{range $k, $v := .}}

                            	<el-form-item label="{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}">
                                                        <span>{{$leftQ}} {{$queryOneFormData}}.{{Low .Name}} {{$rightQ}}</span>
                                                    </el-form-item>
                            	{{end}}
                            {{end}}


                </el-form>
            </el-dialog>
        </div>
    </div>
</template>

<script>
    import {
    api_readmany_{{ .TableNameLow}},
    api_readcount_{{ .TableNameLow}}, api_deleteone_{{ .TableNameLow}},
    api_updateone_{{ .TableNameLow}}, api_updatexxx_{{ .TableNameLow}},
     api_createone_{{ .TableNameLow}},
     isResultOk} from '@/api/api'
    import {utilFormatMoneyFen2Yuan, utilFormatStrTime} from "../api/util";

    export default {
        data() {
            return {
                commandDialogFormVisible: false,
                commandDialogTitle: '{{if .Table.Comment}}{{ .Table.Comment}}{{else}}{{Low .Table.Name}}{{end}}',
                commandFormData: {
                {{with .Columns}}
                                                            	{{range $k, $v := .}}

                                      {{Low .Name}}: '',
                                                            	{{end}}
                                                            {{end}}
                },
                commandType: 1,
                queryOneDialogFormVisible: false,
                queryOneDialogTitle: '{{if .Table.Comment}}{{ .Table.Comment}}{{else}}{{Low .Table.Name}}{{end}}',
                queryOneFormData: {},
                tableData: [],
                offset: 0,
                count: 1000,
                currentPage: 1,
                pageSize: 20,
                pageSizes: [20, 30, 40, 100],
                queryForm: {
                {{with .Columns}}
                                            	{{range $k, $v := .}}

                      {{Low .Name}}: '',
                                            	{{end}}
                                            {{end}}
                },
                rules: {

                {{with .Columns}}
                                                            	{{range $k, $v := .}}

                {{Low .Name}}: [
                                        {required: true, message: "请输入{{if .Comment}}{{Low .Comment}}{{else}}{{Low .Name}}{{end}}", trigger: 'blur'},
                                    ],
                                                            	{{end}}
                                                            {{end}}


                },
            }
        },
        components: {
        },
        created() {
            this.initData();
        },
        methods: {
            async initData() {
                try {
                    this.commandRefresh()
                } catch (err) {
                    console.log('获取数据失败', err);
                }
            },
            handleRowClick(row, column, event) {
                // console.log("handleRowClick : " + row);
            },
            handleQueryOne(index, row) {
                this.queryOneDialogTitle = "查看{{if .Table.Comment}}{{ .Table.Comment}}{{else}}{{Low .Table.Name}}{{end}}"
                this.queryOneDialogFormVisible = true
                this.queryOneFormData = row
            },
            handleDelete(index, row) {

                // console.log(index, row)

                this.$confirm(`删除【 ${ row.{{Low .FirstColumnName}} } 】, 是否继续?`, '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'error'
                }).then(() => {
                    this.deleteOne(row.{{Low .FirstColumnName}})
                }).catch(() => {
                    this.$message({
                        type: 'info',
                        message: '已取消删除'
                    });
                });
            },
            async commandUser(formName) {
                try {
                    this.$refs[formName].validate(async (valid) => {
                        if (valid) {
                           // console.log("formdata : ", this.commandFormData)
                            let res = {}
                            //this.commandFormData.{{Low .FirstColumnName}}
                            if (this.commandType == 1) {
                                res = await api_updateone_{{ .TableNameLow}}(this.commandFormData);
                            } else {
                                res = await api_createone_{{ .TableNameLow}}(this.commandFormData);
                            }
                            if (isResultOk(res)) {
                                this.commandDialogFormVisible = false
                                this.commandRefresh()
                            } else {
                            }
                        } else {
                            this.$message({
                                type: 'error',
                                message: '请输入正确的内容!'
                            });
                        }
                    });
                } catch (err) {
                    this.$message({
                        type: 'error',
                        message: '操作失败!'
                    });
                }
            },
            handleSizeChange(val) {
                console.log(`每页 ${val} 条`);
                this.pageSize = val
                this.commandReadMany()
            },
            handleCurrentChange(val) {
                console.log(`当前页: ${val}`);
                this.currentPage = val;
                this.offset = (val - 1) * this.pageSize;
                this.commandReadMany()
            },
            commandRefresh() {
                this.commandReadCount()
                this.commandReadMany()
            },
            async commandReadMany() {
                const res = await api_readmany_{{ .TableNameLow}}({
                    offset: this.offset,
                    limit: this.pageSize,

                    {{with .Columns}}
                        {{range $k, $v := .}}
                            {{Low .Name}}: this.queryForm.{{Low .Name}},
                        {{end}}
                    {{end}}

                });
                if (isResultOk(res)) {
                    this.tableData = res.body
                }
                console.log(this.tableData)
            },
            async commandReadCount() {
                const res = await api_readcount_{{ .TableNameLow}}({
                {{with .Columns}}
                    {{range $k, $v := .}}
                        {{Low .Name}}: this.queryForm.{{Low .Name}},
                    {{end}}
                {{end}}
                });
                if (isResultOk(res)) {
                    this.count = res.body.count;
                } else {
                }
            },
            async deleteOne({{Low .FirstColumnName}}) {
                const res = await api_deleteone_{{ .TableNameLow}}({

                {{Low .FirstColumnName}}: {{Low .FirstColumnName}}

                });
                if (isResultOk(res)) {
                    this.commandRefresh()
                } else {
                }
            },
            handleQuery() {
                console.log('handleQuery!', this.queryForm);
                this.commandRefresh()
            },
            handleClear() {
                this.$refs['queryForm'].resetFields();
                console.log('handleClear!', this.$refs['queryForm'], this.queryForm);
                this.commandRefresh()
            },
            handleCreate() {
                this.commandDialogFormVisible = true
                this.commandType = 2
                this.commandDialogTitle = "创建{{if .Table.Comment}}{{ .Table.Comment}}{{else}}{{Low .Table.Name}}{{end}}"
                this.commandFormData = {}
                console.log("handleCreate : ")
            },
            handleUpdate(index, row) {
                 this.commandDialogFormVisible = true
                 this.commandType = 1
                 this.commandDialogTitle = "修改{{if .Table.Comment}}{{ .Table.Comment}}{{else}}{{Low .Table.Name}}{{end}}"
                 this.commandFormData = row
                 console.log("handleUpdate : ")
            },
             formatColumnCreateTime(row, column, cellValue, index) {
                 return utilFormatStrTime(cellValue)
             },
             formatColumnMoney(row, column, cellValue, index) {
                 return this.formatMoney(cellValue)
             },
             formatMoney(str) {
                 return utilFormatMoneyFen2Yuan(str)
             },
             formatStrTime(str) {
                 return utilFormatStrTime(str)
             },
        },
    }
</script>

<style lang="less">
    @import '../style/mixin';

    .table_container {
        padding: 20px;
    }


</style>
