//{{.Auth}}
package {{.Package}}

type {{.StructName}} struct {
{{with .Fields}}
	{{range .}}
	{{.Name}}        {{.Type}}     {{.Comment}}
	{{end}}
{{end}}
}