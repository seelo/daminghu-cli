//{{.Auth}}
package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
	"{{.ProjectUri}}/router"
	"{{.ProjectUri}}/filter"
	"github.com/astaxie/beego/logs"
)

func init() {
}

func main() {

	if true {
		beego.SetLogFuncCall(true)
		logs.EnableFuncCallDepth(true)

		beego.BConfig.Log.AccessLogs = true
		beego.BConfig.Log.FileLineNum = true

		logs.SetLogger(logs.AdapterMultiFile, `{"filename":"./logs/{{.ProjectName}}.log","separate":["emergency", "alert", "critical", "error", "warning", "notice", "info", "debug"]}`)

		router.Cfg()
		filter.Cfg()
		beego.Run()
	}
}

