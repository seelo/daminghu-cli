appname = {{.ProjectName}}
servername = daminghu
;graceful = true

;enableadmin = true
;AdminAddr = "localhost"
;AdminPort = 4321

runmode = "dev"

;httpaddr = "127.0.0.1"
[dev]
httpport = {{.Port}}
[prod]
httpport = 12345
[test]
httpport = 8888

include "db.conf"
include "upload.conf"
