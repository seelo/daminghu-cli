//{{.Auth}}
package model

import (
	"github.com/astaxie/beego"
)

type baseModule struct {
}

func (b *baseModule) CheckError(err error) {
	if err != nil {
		beego.Critical(err)
		panic(err)
	}
}

func (b *baseModule) Debug(v ...interface{}) {
	beego.Info(v)
}

func (b *baseModule) Error(v ...interface{}) {
	beego.Error(v)
}


