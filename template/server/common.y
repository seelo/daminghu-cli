//{{.Auth}}
package common

import (
	"time"
	"github.com/allegro/bigcache"
	"github.com/astaxie/beego"
)

var (
	PORT = beego.AppConfig.String("httpport")

	MYSQLCONN = beego.AppConfig.String("mysqlconnstr") + beego.AppConfig.String("mysqlconnparam")

	URL_PRE           = beego.AppConfig.String("url_pre")
	UPLOAD_PATH       = beego.AppConfig.String("upload_path")
	SHORT_UPLOAD_PATH = beego.AppConfig.String("short_upload_path")
	SQLMAPPATH        = beego.AppConfig.String("sqlmappath")
	SQLMAPEXT         = beego.AppConfig.String("sqlmapext")
	SQLTEMPLATEPATH   = beego.AppConfig.String("sqltemplatepath")
	SQLTEMPLATEEXT    = beego.AppConfig.String("sqltemplateext")
)

const ()

var Cache *bigcache.BigCache

func init() {
	Cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(10 * time.Minute))
}


