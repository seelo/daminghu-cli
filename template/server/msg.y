//{{.Auth}}
package msg

const (
	RESULT_SUCCERR   = 0
	RESULT_ERROR     = -1
	RESULT_NO_TOKEN  = -10
	MESSAGE_SUCCESS  = "操作成功"
	MESSAGE_ERROR    = "操作失败"
	MESSAGE_NO_TOKEN = "请先登录后再操作"

	TYPE_C = 1
	TYPE_R = 2
	TYPE_U = 3
	TYPE_D = 4
)

type Response struct {
	Result  int         `json:"result"`
	OptType int         `json:"opttype"`
	Message string      `json:"message"`
	Body    interface{} `json:"body"`
	Token   string      `json:"token"`
}

type builder struct {
	Response
	data map[string]interface{}
}

func ResponseBuilder() *builder {
	return &builder{}
}

func (b *builder) OptC() *builder {
	b.OptType = TYPE_C
	return b
}

func (b *builder) OptR() *builder {
	b.OptType = TYPE_R
	return b
}

func (b *builder) OptU() *builder {
	b.OptType = TYPE_U
	return b
}

func (b *builder) OptD() *builder {
	b.OptType = TYPE_D
	return b
}

func (b *builder) BodyValue(key string, value interface{}) *builder {
	if b.data == nil {
		b.data = make(map[string]interface{})
	}
	b.data[key] = value
	return b
}

func (b *builder) Build() Response {
	if b.data != nil {
		b.Response.Body = b.data
	}
	return b.Response
}

type BaseBody struct {
	data map[string]interface{}
}

func (b *BaseBody) Body() map[string]interface{} {
	return b.data
}

func Res(key string, value interface{}) *BaseBody {
	data := make(map[string]interface{})
	data[key] = value
	return &BaseBody{data}
}

func (b *builder) ErrorResponse() *builder {
	b.Response = Response{Result: RESULT_ERROR, Message: MESSAGE_ERROR}
	return b
}

func (b *builder) NoTokenResponse() *builder {
	b.Response = Response{Result: RESULT_NO_TOKEN, Message: MESSAGE_NO_TOKEN}
	return b
}

func (b *builder) DBErrorResponse() *builder {
	b.Response = Response{Result: RESULT_ERROR, Message: "DB Error"}
	return b
}

func (b *builder) ErrorResponseDesc(errDesc string) *builder {
	b.Response = Response{Result: RESULT_ERROR, Message: errDesc}
	return b
}

func (b *builder) SuccessResponse() *builder {
	b.Response = Response{Result: RESULT_SUCCERR, Message: MESSAGE_SUCCESS}
	return b
}

//func ErrorResponse() Response {
//	return Response{Result: RESULT_ERROR, Message: MESSAGE_ERROR}
//}
//
//func NoTokenResponse() Response {
//	return Response{Result: RESULT_NO_TOKEN, Message: MESSAGE_NO_TOKEN}
//}
//
//func DBErrorResponse() Response {
//	return Response{Result: RESULT_ERROR, Message: "DB Error"}
//}
//
//func ErrorResponseDesc(errDesc string) Response {
//	return Response{Result: RESULT_ERROR, Message: errDesc}
//}
//
//func SuccessResponse() Response {
//	return Response{Result: RESULT_SUCCERR, Message: MESSAGE_SUCCESS}
//}

