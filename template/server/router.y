//{{.Auth}}
package {{.Package}}

import (
	"github.com/astaxie/beego"
	"{{.ProjectUri}}/api"
)

func Cfg() {

	ns := beego.NewNamespace("/api",
		//此处正式版时改为验证加密请求
		//beego.NSCond(func(ctx *context.Context) bool {
		//	logs.Info("UserAgent", ctx.Request.UserAgent())
		//	logs.Info("Host", ctx.Request.Host)
		//	//if ctx.Request.Host != "localhost:12345" {
		//	//	return false
		//	//}
		//	//if ua := ctx.Input.Request.UserAgent(); ua != "" {
		//	//	return true
		//	//}
		//	//return false
		//	return true
		//}),

beego.NSRouter("/readlogin", &api.LOGIN_CONTROLLER{}, "post:ReadLogin"),

		{{with .Tables}}

		    {{range $k, $v := .}}
		    beego.NSNamespace("/{{ReplaceUnderLineLow .Name}}",

            			beego.NSRouter("/readcount", &api.{{Upper .Name}}_CONTROLLER{}, "get:ReadCount"),
            			beego.NSRouter("/deleteone", &api.{{Upper .Name}}_CONTROLLER{}, "get:DeleteOne"),
            			beego.NSRouter("/readone", &api.{{Upper .Name}}_CONTROLLER{}, "get:ReadOne"),
            			beego.NSRouter("/createone", &api.{{Upper .Name}}_CONTROLLER{}, "post:CreateOne"),
            			beego.NSRouter("/updateone", &api.{{Upper .Name}}_CONTROLLER{}, "post:UpdateOne"),
            			beego.NSRouter("/updatexxx", &api.{{Upper .Name}}_CONTROLLER{}, "post:UpdateXXX"),
            			beego.NSRouter("/readmany", &api.{{Upper .Name}}_CONTROLLER{}, "get:ReadMany")),

        	{{end}}
        {{end}}
	)

	beego.AddNamespace(ns)

}
