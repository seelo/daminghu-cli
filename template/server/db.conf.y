;mysqlconnparam = "?charset=utf8&parseTime=true&loc=Local&timeout=10s"
sqlmapext = ".xsql"
sqltemplateext = ".stpl"

[dev]
mysqlconnstr = "{{.MySqlConn}}"
sqlmappath = "./sql/mysql/"
sqltemplatepath = "./sql/mysql/"
[prod]
mysqlconnstr = ""
sqlmappath = "./sql/mysql/"
sqltemplatepath = "./sql/mysql/"