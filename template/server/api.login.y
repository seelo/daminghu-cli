//{{.Auth}}
package api

import (
	"{{.ProjectUri}}/common"
	"crypto/md5"
	"encoding/hex"
	"{{.ProjectUri}}/msg"
)

type LOGIN_CONTROLLER struct {
	baseController
}

//curl -X POST "http://localhost:12345/api/user/login" -H "accept: application/json"  -d "username=user06&password=1112221"
func (this *LOGIN_CONTROLLER) ReadLogin() {
	username := this.GetString("username")
	password := this.GetString("password")

	bs := md5.Sum([]byte(password))
	pwd := hex.EncodeToString(bs[:])
	//sql := "SELECT userid,userLoginName,usertype FROM t_user_info where userloginname = ?userloginname and passwd = ?passwd "
	paramMap := map[string]interface{}{}
	paramMap["userloginname"] = username
	paramMap["passwd"] = pwd
	this.Debug("paramMap", paramMap)
	//results, err := db.Engine.SQL(sql, &paramMap).QueryString()
	//if err != nil {
	//	logs.Error(err.Error())
	//	this.Data["json"] = msg.ResponseBuilder().ErrorResponseDesc("用户名或密码错误").Build()
	//} else {
	//	this.Debug(results)
	//	if results != nil && len(results) > 0 && results[0]["userid"] != "" {
	//		res := msg.ResponseBuilder().OptR().SuccessResponse().Build()
	//		res.Body = results[0]
	//		res.Token = this.CreateToken()
	//		common.Cache.Set(res.Token, []byte(results[0]["userid"]))
	//		//this.SetSession("token", res.Token)
	//		this.Data["json"] = &res
	//	} else {
	//		this.Data["json"] = msg.ResponseBuilder().ErrorResponseDesc("用户名或密码错误").Build()
	//	}
	//}

	res := msg.ResponseBuilder().OptR().SuccessResponse().Build()
	au := AdminUser{"yasenagat-123456"}
	res.Body = au
	res.Token = this.CreateToken()
	common.Cache.Set(res.Token, []byte(au.UserId))
	//this.SetSession("token", res.Token)
	this.Data["json"] = &res
	this.ServeJSON()
}

type AdminUser struct {
	UserId string `json:"userid"`
}

