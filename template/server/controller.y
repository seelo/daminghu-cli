//{{.Auth}}
package {{.Package}}

import (
	"{{.ProjectUri}}/model"
	"{{.ProjectUri}}/msg"
	"{{.ProjectUri}}/db"
)

type {{.ControllerName}} struct {
	baseController
	model.{{.ModelName}}
}

//curl "{{.PreUrl}}/api/{{ReplaceUnderLineLow .TableNameLow}}/readmany?offset=0&limit=2&token=1"
func (this *{{.ControllerName}}) ReadMany() {

	defer this.TryRecover()

    paramMap := this.getReadManyParam()
	this.Debug("paramMap", paramMap)

	datas, err := this.{{.ModelName}}.ReadMany(paramMap)

	this.CheckError(err)
	//value, ok := datas.([]db.{{.TableNameUpper}})
    	//if ok {
    	//	this.Debug(value)
    	//}
    this.Debug("datas", datas)
	res := msg.ResponseBuilder().OptR().SuccessResponse().Build()
	res.Body = datas
	this.Data["json"] = &res

	this.Debug("Response", res)
	this.ServeJSON()
}

func (this *{{.ControllerName}}) getReadManyParam() ( map[string]interface{}) {

	yoffset := this.GetString("offset")
    	ylimit := this.GetString("limit")
    	{{with .Columns}}
        	{{range $k, $v := .}}
        	{{ConvertKeyWord .Name}} := this.GetString("{{Low .Name}}", "")
        	{{end}}
        {{end}}

    	paramMap := map[string]interface{}{}
    	paramMap["offset"] = yoffset
    	paramMap["limit"] = ylimit
    	{{with .Columns}}
            	{{range $k, $v := .}}
            	paramMap["{{Low .Name}}"] = {{ConvertKeyWord .Name}}
            	{{end}}
            {{end}}

	return  paramMap
}

//curl "{{.PreUrl}}/api/{{ReplaceUnderLineLow .TableNameLow}}/count?token=1"
func (this *{{.ControllerName}}) ReadCount() {

	defer this.TryRecover()

    paramMap := this.getReadCountParam()
	this.Debug("paramMap", paramMap)

	count, err := this.{{.ModelName}}.ReadCount(paramMap)
    	this.CheckError(err)

	this.Debug("count", count)
	res := msg.ResponseBuilder().OptR().SuccessResponse().Build()
	res.Body = msg.Res("count", count).Body()
	this.Data["json"] = &res
	this.Debug("Response", res)
	this.ServeJSON()
}

func (this *{{.ControllerName}}) getReadCountParam() ( map[string]interface{}) {

	{{with .Columns}}
        	{{range $k, $v := .}}
        	{{ConvertKeyWord .Name}} := this.GetString("{{Low .Name}}", "")
        	{{end}}
        {{end}}

    	paramMap := map[string]interface{}{}
    	{{with .Columns}}
                	{{range $k, $v := .}}
                	paramMap["{{Low .Name}}"] = {{ConvertKeyWord .Name}}
                	{{end}}
                {{end}}
	return  paramMap
}

//curl "{{.PreUrl}}/api/{{ReplaceUnderLineLow .TableNameLow}}/readone?{{Low .FirstColumnName}}=aaa&token=1"
func (this *{{.ControllerName}}) ReadOne() {

	defer this.TryRecover()

	paramMap := this.getReadOneParam()
	this.Debug("paramMap", paramMap)

	var data db.{{.TableNameUpper}}
    	data, err := this.{{.ModelName}}.ReadOne(paramMap)
    	this.CheckError(err)
	this.Debug("data", data)

	res := msg.ResponseBuilder().OptR().SuccessResponse().Build()
    	res.Body = data
    	this.Data["json"] = &res
    	this.Debug("Response", res)
    	this.ServeJSON()
}

func (this *{{.ControllerName}}) getReadOneParam() ( map[string]interface{}) {

	{{ConvertKeyWord .FirstColumnName}} := this.GetString("{{Low .FirstColumnName}}", "")

    	paramMap := map[string]interface{}{}
    	paramMap["{{Low .FirstColumnName}}"] = {{ConvertKeyWord .FirstColumnName}}
	return  paramMap
}

//curl "{{.PreUrl}}/api/{{ReplaceUnderLineLow .TableNameLow}}/deleteone?{{Low .FirstColumnName}}=123&token=1"
func (this *{{.ControllerName}}) DeleteOne() {

	defer this.TryRecover()

	paramMap := this.getDeleteOneParam()
	this.Debug("paramMap", paramMap)

	i, err := this.{{.ModelName}}.DeleteOne(paramMap)
    	this.CheckError(err)

	res := msg.ResponseBuilder().OptU().SuccessResponse().Build()
    	if i < 0 {
    		this.Debug("change zero!")
    	}
	this.Data["json"] = &res
	this.Debug("Response", res)
	this.ServeJSON()
}

func (this *{{.ControllerName}}) getDeleteOneParam() ( map[string]interface{}) {

	{{ConvertKeyWord .FirstColumnName}} := this.GetString("{{Low .FirstColumnName}}")

    	paramMap := map[string]interface{}{}
    	paramMap["{{Low .FirstColumnName}}"] = {{ConvertKeyWord .FirstColumnName}}
	return  paramMap
}

//curl -X POST "{{.PreUrl}}/{{ReplaceUnderLineLow .TableNameLow}}/user/createone" -d "{{Low .FirstColumnName}}=123&{{Low .SecondColumnName}}=123&token=1"
func (this *{{.ControllerName}}) CreateOne() {

	defer this.TryRecover()

	paramMap := this.getCreateOneParam()
	this.Debug("paramMap", paramMap)

	i, err := this.{{.ModelName}}.CreateOne(paramMap)
    	this.CheckError(err)

	res := msg.ResponseBuilder().OptU().SuccessResponse().Build()
    	if i < 0 {
    		this.Debug("change zero!")
    	}
	this.Data["json"] = &res
	this.Debug("Response", res)
	this.ServeJSON()

}

func (this *{{.ControllerName}}) getCreateOneParam() ( map[string]interface{}) {

	{{with .Columns}}
            	{{range $k, $v := .}}
            	{{ConvertKeyWord .Name}} := this.GetString("{{Low .Name}}", "")
            	{{end}}
            {{end}}

    	paramMap := map[string]interface{}{}
    	{{with .Columns}}
                    	{{range $k, $v := .}}
                    	paramMap["{{Low .Name}}"] = {{ConvertKeyWord .Name}}
                    	{{end}}
                    {{end}}
	return  paramMap
}

//curl -X POST "{{.PreUrl}}/api/{{ReplaceUnderLineLow .TableNameLow}}/updateone" -d "{{Low .FirstColumnName}}=123&{{Low .SecondColumnName}}=123&token=1"
func (this *{{.ControllerName}}) UpdateOne() {

	defer this.TryRecover()

	paramMap := this.getUpdateOneParam()
	this.Debug("paramMap", paramMap)

	i, err := this.{{.ModelName}}.UpdateOne(paramMap)
    	this.CheckError(err)

	res := msg.ResponseBuilder().OptU().SuccessResponse().Build()
    	if i < 0 {
    		this.Debug("change zero!")
    	}
	this.Data["json"] = &res
	this.Debug("Response", res)
	this.ServeJSON()

}

func (this *{{.ControllerName}}) getUpdateOneParam() ( map[string]interface{}) {

	{{with .Columns}}
                	{{range $k, $v := .}}
                	{{ConvertKeyWord .Name}} := this.GetString("{{Low .Name}}", "")
                	{{end}}
                {{end}}

    	paramMap := map[string]interface{}{}
    	{{with .Columns}}
                    	{{range $k, $v := .}}
                    	paramMap["{{Low .Name}}"] = {{ConvertKeyWord .Name}}
                    	{{end}}
                    {{end}}
	return  paramMap
}

//curl -X POST "{{.PreUrl}}/api/{{ReplaceUnderLineLow .TableNameLow}}/updatexxx" -d "a=123"
func (this *{{.ControllerName}}) UpdateXXX() {

	defer this.TryRecover()

	paramMap := this.getUpdateOneParam()
	this.Debug("paramMap", paramMap)

	i, err := this.{{.ModelName}}.UpdateXXX(paramMap)
    	this.CheckError(err)

	res := msg.ResponseBuilder().OptU().SuccessResponse().Build()
    	if i < 0 {
    		this.Debug("change zero!")
    	}
	this.Data["json"] = &res
	this.Debug("Response", res)
	this.ServeJSON()

}

func (this *{{.ControllerName}}) getUpdateXXXParam() ( map[string]interface{}) {

	{{ConvertKeyWord .FirstColumnName}} := this.GetString("{{Low .FirstColumnName}}", "")
        {{ConvertKeyWord .SecondColumnName}} := this.GetString("{{Low .SecondColumnName}}", "")


    	paramMap := map[string]interface{}{}
    	paramMap["{{Low .FirstColumnName}}"] = {{ConvertKeyWord .FirstColumnName}}
    	paramMap["{{Low .SecondColumnName}}"] = {{ConvertKeyWord .SecondColumnName}}
	return  paramMap
}
