//{{.Auth}}
package {{.Package}}

import (
	"{{.ProjectUri}}/db"
)

type {{.ModelName}} struct {
	baseModule
}

func (this *{{.ModelName}}) ReadCount(paramMap map[string]interface{}) (int64, error) {

	this.Debug("paramMap", paramMap)

	var count int64
	sql := "{{.TableNameLow}}.count.stpl"
	has, err := db.Engine.SqlTemplateClient(sql, &paramMap).Get(&count)
if err != nil {
		this.Error(err)
	}
	if !has {
		count = 0
	}

	this.Debug("count", count)
	return count, err
}

func (this *{{.ModelName}}) ReadMany(paramMap map[string]interface{}) ([]db.{{.TableNameUpper}}, error) {

	this.Debug("paramMap", paramMap)
	var datas []db.{{.TableNameUpper}}

	sql := "{{.TableNameLow}}.read.stpl"
	err := db.Engine.SqlTemplateClient(sql, &paramMap).Find(&datas)

	this.CheckError(err)

	this.Debug("datas", datas)
	if len(datas) == 0 {
		datas = make([]db.{{.TableNameUpper}}, 0)
	}
	return datas, err
}

func (this *{{.ModelName}}) ReadOne(paramMap map[string]interface{}) (db.{{.TableNameUpper}}, error) {

	this.Debug("paramMap", paramMap)
	var data db.{{.TableNameUpper}}

	sql := "read-{{.TableNameLow}}-by-{{Low .FirstColumnName}}"
	has, err := db.Engine.SqlMapClient(sql, &paramMap).Get(&data)
	this.CheckError(err)

	this.Debug("data", has, data)
	if !has {
		return db.{{.TableNameUpper}}{}, err
	}
	return data, nil
}

func (this *{{.ModelName}}) DeleteOne(paramMap map[string]interface{}) (int64, error) {

	this.Debug("paramMap", paramMap)

	sql := "delete-{{.TableNameLow}}-by-{{Low .FirstColumnName}}"
	result, err := db.Engine.SqlMapClient(sql, &paramMap).Execute()
	this.CheckError(err)

	return result.RowsAffected()
}

func (this *{{.ModelName}}) CreateOne(paramMap map[string]interface{}) (int64, error) {

	this.Debug("paramMap", paramMap)

	sql := "create-{{.TableNameLow}}"
	result, err := db.Engine.SqlMapClient(sql, &paramMap).Execute()
	this.CheckError(err)

	return result.RowsAffected()
}

func (this *{{.ModelName}}) UpdateOne(paramMap map[string]interface{}) (int64, error) {

	this.Debug("paramMap", paramMap)

	sql := "update-{{.TableNameLow}}"
	result, err := db.Engine.SqlMapClient(sql, &paramMap).Execute()
	this.CheckError(err)
	return result.RowsAffected()
}

func (this *{{.ModelName}}) UpdateXXX(paramMap map[string]interface{}) (int64, error) {

	this.Debug("paramMap", paramMap)

	sql := "update-{{.TableNameLow}}-SecondColumnName-by-{{Low .FirstColumnName}}"
	result, err := db.Engine.SqlMapClient(sql, &paramMap).Execute()
	this.CheckError(err)

	return result.RowsAffected()
}
