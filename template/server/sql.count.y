SELECT 
 count(1) count
 FROM 
 {{.TableNameLow}} T where 1=1
 {{with .Columns}}
   	{{range .}}
   	{% if {{Low .Name}} %} and {{Low .Name}} = ?{{Low .Name}} {% endif %}
   	{{end}}
   {{end}}