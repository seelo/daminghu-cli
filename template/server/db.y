//{{.Auth}}
package db

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/xormplus/xorm"
	"github.com/astaxie/beego"
	"{{.ProjectUri}}/common"
)

var (
	Engine *xorm.Engine
)

func init() {
	beego.Debug("db init")
	var err error
	Engine, err = xorm.NewEngine("mysql", common.MYSQLCONN)
	if err != nil {
		beego.Error(err.Error())
		panic(err)
	}
	err = Engine.Ping()
	if err != nil {
		beego.Error(err.Error())
		panic(err)
	}

	Engine.ShowSQL(true)

	//注册SqlMap配置，xsql格式
	err = Engine.RegisterSqlMap(xorm.XSql(common.SQLMAPPATH, common.SQLMAPEXT))

	if err != nil {
		beego.Error(err)
		panic(err)
		return
	}

	err = Engine.RegisterSqlTemplate(xorm.Pongo2(common.SQLTEMPLATEPATH, common.SQLTEMPLATEEXT))
	if err != nil {
		beego.Error(err)
		panic(err)
		return
	}
}



