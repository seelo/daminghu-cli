{{$c := .Columns}}
SELECT
 {{with .Columns}}
 	{{range $k, $v := .}}
    	{{Low .Name}}{{IsLast $k $c}}
    	{{end}}
 {{end}}
 FROM 
 {{.TableNameLow}} where 1=1
  {{with .Columns}}
  	{{range .}}
  	{% if {{Low .Name}} %} and {{Low .Name}} = ?{{Low .Name}} {% endif %}
  	{{end}}
  {{end}}
limit ?offset,?limit
