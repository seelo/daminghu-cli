//{{.Auth}}
package api

import (
	"github.com/astaxie/beego"
	"github.com/google/uuid"
	"strings"
)

type baseController struct {
	beego.Controller
}

func (b *baseController) CheckError(err error) {
	if err != nil {
		beego.Critical(err)
		panic(err)
	}
}

func (b *baseController) TryRecover() {
	beego.Debug("TryRecover")
	//if err := recover(); err != nil {
	//	beego.Error(err)
	//	b.Data["json"] = module.ErrorResponse()
	//	b.ServeJSON()
	//	return
	//}
}

func (b *baseController) GetMsgId() string {
	return b.Ctx.Input.Header("messageid")
}

func (b *baseController) Debug(v ...interface{}) {
	beego.Info("#", b.GetMsgId(), "#", v)
}

func (b *baseController) CreateToken() string {
	token := strings.Replace(uuid.New().String(), "-", "", -1)
	beego.Info("CreateToken", token)
	return token
}

