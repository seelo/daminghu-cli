{{$c := .Columns}}
-- id: create-{{.TableNameLow}}
INSERT INTO {{.TableNameLow}} (
{{with .Columns}}
	{{range $k, $v := .}}
	{{Low .Name}}{{IsLast $k $c}}
	{{end}}
{{end}}
) VALUES (
{{with .Columns}}
	{{range $k, $v := .}}
	?{{Low .Name}}{{IsLast $k $c}}
	{{end}}
{{end}}
)

-- id: update-{{.TableNameLow}}
UPDATE
  {{.TableNameLow}} set
  {{with .Columns}}
  	{{range $k, $v := .}}
  	{{Low .Name}} = ?{{Low .Name}}{{IsLast $k $c}}
  	{{end}}
  {{end}}
WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}

-- id: read-{{.TableNameLow}}-by-{{.FirstColumnName}}
SELECT * FROM {{.TableNameLow}} WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}

-- id: delete-{{.TableNameLow}}-by-{{.FirstColumnName}}
DELETE FROM {{.TableNameLow}} WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}

-- id: update-{{.TableNameLow}}-{{.SecondColumnName}}-by-{{.FirstColumnName}}
UPDATE
  {{.TableNameLow}} set
  	{{.SecondColumnName}} = ?{{.SecondColumnName}}
WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}