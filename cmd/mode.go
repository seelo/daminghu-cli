package cmd

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/handler"
	demoserver "gitee.com/godY/daminghu-cli/handler/demo/server"
	"gitee.com/godY/daminghu-cli/handler/dev/server"
	"gitee.com/godY/daminghu-cli/handler/dev/web"
)

func modeFunc(modeValue string) {
	ModeFuncMap[modeValue](modeValue)
}

func modeDevFunc(value string) {
	AddServerHandler(server.CreateDirHandler)
	if !cfg.Cfg.LocalTemplate {
		AddServerHandler(server.DownloadTemplateHandler)
	}
	AddServerHandler(server.CreateBaseProjectHandler)
	AddServerHandler(server.CreateRouterHandler)
	AddServerHandler(server.CreateStructHandler)
	AddServerHandler(server.CreateControllerHandler)
	AddServerHandler(server.CreateSqlHandler)
	AddServerHandler(server.CreateModelHandler)

	AddWebHandler(web.CreateDirHandler)
	AddWebHandler(web.DownloadBaseProjectHandler)
	if !cfg.Cfg.LocalTemplate {
		AddWebHandler(web.DownloadTemplateHandler)
	}
	AddWebHandler(web.CreateApiHandler)
	AddWebHandler(web.CreateEnvHandler)
	AddWebHandler(web.CreateRouterHandler)
	AddWebHandler(web.CreatePageHandler)
	AddWebHandler(web.CreateMainHandler)
	AddWebHandler(web.CreateHomeHandler)

	AddAfterHandler(handler.DeleteTempHandler)
}

func modeDemoFunc(value string) {
	AddServerHandler(demoserver.CreateHandler)
}
