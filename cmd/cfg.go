package cmd

import (
	"encoding/json"
	"gitee.com/godY/daminghu-cli/cfg"
	"github.com/astaxie/beego"
	"io/ioutil"
)

func cfgFunc(cfgpath string) {

	bytes, e := ioutil.ReadFile(cfgpath)

	if e != nil {
		panic(e)
	}
	c := cfg.Config{}
	e = json.Unmarshal(bytes, &c)

	if e != nil {
		panic(e)
	}
	cfg.Cfg = c
}

func cfgAfterFunc(value string) {
	beego.Debug("cfgAfterFunc", value)
	//db.InitDb()
}
