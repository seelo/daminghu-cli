package cmd

import (
	"encoding/json"
	"gitee.com/godY/daminghu-cli/db"
	"gitee.com/godY/daminghu-cli/handler/demo"
	"github.com/astaxie/beego"
	"io/ioutil"
)

func jsondataBeforeFunc(filepath string) {
	if filepath == "" {
		db.InitDb()
	}
}
func jsondataFunc(filepath string) {
	beego.Debug("jsondataFunc", filepath)
	if filepath != "" {

		bytes, e := ioutil.ReadFile(filepath)

		if e != nil {
			panic(e)
		}
		req := demo.CreateReq{}
		e = json.Unmarshal(bytes, &req)

		if e != nil {
			panic(e)
		}
		db.Tables = req.Tables
	}
}
