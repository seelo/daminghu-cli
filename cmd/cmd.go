package cmd

var (
	CmdBeforeFuncMap = make(map[string]cmdFunc)
	CmdFuncMap       = make(map[string]cmdFunc)
	CmdAfterFuncMap  = make(map[string]cmdFunc)
	BuildFuncMap     = make(map[string]valueFunc)
	ModeFuncMap      = make(map[string]valueFunc)
)

func init() {

	CmdFuncMap["build"] = buildFunc
	CmdFuncMap["cfg"] = cfgFunc
	CmdFuncMap["mode"] = modeFunc
	CmdFuncMap["jsondata"] = jsondataFunc
	CmdFuncMap["email"] = emailFunc

	CmdBeforeFuncMap["build"] = buildBeforeFunc
	CmdBeforeFuncMap["jsondata"] = jsondataBeforeFunc

	CmdAfterFuncMap["build"] = buildAfterFunc
	CmdAfterFuncMap["cfg"] = cfgAfterFunc

	BuildFuncMap["all"] = buildAllFunc
	BuildFuncMap["svr"] = buildSvrFunc
	BuildFuncMap["web"] = buildWebFunc

	ModeFuncMap["dev"] = modeDevFunc
	ModeFuncMap["demo"] = modeDemoFunc
}

type cmdFunc func(value string)

type valueFunc func(value string)

func CheckCmds(cmds []string, values []string) {
	for index, cmd := range cmds {
		if _, ok := CmdFuncMap[cmd]; ok {

			if cmd == "build" {
				if _, ok := BuildFuncMap[values[index]]; ok {
				} else {
					panic(`build type not support`)
				}
			}

			if cmd == "mode" {
				if _, ok := ModeFuncMap[values[index]]; ok {
				} else {
					panic(`mode value not support`)
				}
			}
		} else {
			panic("cmd not support")
		}
	}

}
