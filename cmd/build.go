package cmd

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/db"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"os"
)

func buildBeforeFunc(value string) {

	beego.Debug("buildBeforeFunc", value)

	beego.Debug("handle table num ", len(db.Tables))

	//if mode == "dev" {
	//
	//	AddServerHandler(server.CreateDirHandler)
	//	if !cfg.Cfg.LocalTemplate {
	//		AddServerHandler(server.DownloadTemplateHandler)
	//	}
	//	AddServerHandler(server.CreateBaseProjectHandler)
	//	AddServerHandler(server.CreateRouterHandler)
	//	AddServerHandler(server.CreateStructHandler)
	//	AddServerHandler(server.CreateControllerHandler)
	//	AddServerHandler(server.CreateSqlHandler)
	//	AddServerHandler(server.CreateModelHandler)
	//
	//	AddWebHandler(web.CreateDirHandler)
	//	AddWebHandler(web.DownloadBaseProjectHandler)
	//	if !cfg.Cfg.LocalTemplate {
	//		AddWebHandler(web.DownloadTemplateHandler)
	//	}
	//	AddWebHandler(web.CreateApiHandler)
	//	AddWebHandler(web.CreateEnvHandler)
	//	AddWebHandler(web.CreateRouterHandler)
	//	AddWebHandler(web.CreatePageHandler)
	//	AddWebHandler(web.CreateMainHandler)
	//	AddWebHandler(web.CreateHomeHandler)
	//
	//	AddAfterHandler(handler.DeleteTempHandler)
	//} else {
	//}

}

func buildAfterFunc(value string) {
	beego.Debug("buildAfterFunc", value)
	os.RemoveAll(cfg.Cfg.GetServerTempPath())
	os.RemoveAll(cfg.Cfg.GetWebTempPath())
}

func buildFunc(value string) {

	if f, ok := BuildFuncMap[value]; ok {
		f(value)
	} else {

	}
}

func buildAllFunc(value string) {
	tables := db.Tables
	buildBefore(tables)
	buildSvr(tables)
	buildWeb(tables)
	buildAfter(tables)
}

func buildSvrFunc(value string) {
	tables := db.Tables
	buildBefore(tables)
	buildSvr(tables)
	buildAfter(tables)
}

func buildWebFunc(value string) {
	tables := db.Tables
	buildBefore(tables)
	buildWeb(tables)
	buildAfter(tables)
}

func buildWeb(tables []*common.Table) {
	logs.Info("Start Build Web Project..............")
	handlerTables(tables, webHanders)
	handleOneTable(tables, webHanders)
	logs.Info("Build Web Project Complete!")
}

func buildSvr(tables []*common.Table) {
	logs.Info("Start Build Svr Project..............")
	handlerTables(tables, serverHandlers)
	handleOneTable(tables, serverHandlers)
	logs.Info("Build Svr Project Complete!")
}

func buildBefore(tables []*common.Table) {
	logs.Info("Start Before Handler..............")
	handlerTables(tables, beforeHandlers)
	handleOneTable(tables, beforeHandlers)
	logs.Info("Invoke Before Handler Complete!")
}

func buildAfter(tables []*common.Table) {
	logs.Info("Start After Handler..............")
	handlerTables(tables, afterHandlers)
	handleOneTable(tables, afterHandlers)
	logs.Info("Invoke After Handler Complete!")
}
