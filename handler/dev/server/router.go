package server

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type RouterInfo struct {
	handler.BaseInfo
}

var CreateRouterHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("CreateRouterHandler")

	info := RouterInfo{}
	info.SetUp()
	info.Package = "router"
	info.Tables = table

	createServerFile("router/router.go", "router.y", info)
}

//const RouterTemp = `package {{.Package}}
//
//import (
//	"github.com/astaxie/beego"
//	"gitee.com/godY/easygo/wxfileupload/api"
//)
//
//func Cfg() {
//
//	ns := beego.NewNamespace("/api",
//		//此处正式版时改为验证加密请求
//		//beego.NSCond(func(ctx *context.Context) bool {
//		//	logs.Info("UserAgent", ctx.Request.UserAgent())
//		//	logs.Info("Host", ctx.Request.Host)
//		//	//if ctx.Request.Host != "localhost:12345" {
//		//	//	return false
//		//	//}
//		//	//if ua := ctx.Input.Request.UserAgent(); ua != "" {
//		//	//	return true
//		//	//}
//		//	//return false
//		//	return true
//		//}),
//
//
//		{{with .Tables}}
//
//		    {{range $k, $v := .}}
//		    beego.NSNamespace("/{{ReplaceUnderLineLow .Name}}",
//
//            			beego.NSRouter("/count", &api.{{Upper .Name}}_CONTROLLER{}, "get:Count"),
//            			beego.NSRouter("/deleteone", &api.{{Upper .Name}}_CONTROLLER{}, "get:DeleteOne"),
//            			beego.NSRouter("/readone", &api.{{Upper .Name}}_CONTROLLER{}, "get:ReadOne"),
//            			beego.NSRouter("/createone", &api.{{Upper .Name}}_CONTROLLER{}, "post:CreateOne"),
//            			beego.NSRouter("/updateone", &api.{{Upper .Name}}_CONTROLLER{}, "post:UpdateOne"),
//            			beego.NSRouter("/updatexxx", &api.{{Upper .Name}}_CONTROLLER{}, "post:UpdateXXX"),
//            			beego.NSRouter("/list", &api.{{Upper .Name}}_CONTROLLER{}, "get:List")),
//
//        	{{end}}
//        {{end}}
//	)
//
//	beego.AddNamespace(ns)
//
//}
//
//`
