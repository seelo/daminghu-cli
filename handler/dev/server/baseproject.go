package server

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type BaseProjectInfo struct {
	handler.BaseInfo
}

var CreateBaseProjectHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("CreateBaseProjectHandler")
	info := BaseProjectInfo{}
	info.SetUp()
	//beego.Debug(info.MySqlConn)
	createServerFile("api/base.go", "api.base.y", info)
	createServerFile("api/login.go", "api.login.y", info)
	createServerFile("cmd/main.go", "main.y", info)
	createServerFile("cmd/conf/app.conf", "app.conf.y", info)
	createServerFile("cmd/conf/db.conf", "db.conf.y", info)
	createServerFile("cmd/conf/upload.conf", "upload.conf.y", info)
	createServerFile("common/common.go", "common.y", info)
	createServerFile("db/db.go", "db.y", info)
	createServerFile("filter/filter.go", "filter.y", info)
	createServerFile("model/base.go", "model.base.y", info)
	createServerFile("msg/base.go", "msg.y", info)
}
