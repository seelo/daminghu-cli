package server

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"os"
)

var CreateDirHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("CreateDirHandler")

	//mask := syscall.Umask(0)
	//defer syscall.Umask(mask)
	filemode := os.ModePerm

	os.MkdirAll(cfg.Cfg.GetServerProjcetPath(), filemode)

	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"api", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"cmd", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"cmd/logs", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"cmd/conf", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"cmd/sql/mysql", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"common", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"db", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"filter", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"model", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"msg", filemode)
	os.MkdirAll(cfg.Cfg.GetServerProjcetPath()+"router", filemode)
}
