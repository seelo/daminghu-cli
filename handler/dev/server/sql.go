package server

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/db"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"strings"
)

type SqlInfo struct {
	SqlName string
	Fields  []Field
	handler.BaseInfo
}

var CreateSqlHandler handler.HandlerOneTableFunc = func(table *common.Table, columns []*common.Column) {
	beego.Debug("CreateSqlHandler")

	fileds := make([]Field, 0)
	fieldCommont := ""
	fieldName := ""
	for _, column := range columns {

		fieldCommont = "`db:\"" + column.Name + "\"` //" + handler.ConvertComment(column.Comment)
		//moduleCommont = "`json:\"" + strings.ToLower(column.Name) + "\"`"
		fieldName = strings.ToUpper(column.Name[:1]) + strings.ToLower(column.Name[1:])
		fieldType, ok := db.DBTYPE[column.SQLType.Name]
		if !ok {
			fieldType = "string"
		}

		fileds = append(fileds, Field{fieldName, fieldType, fieldCommont})
	}

	info := SqlInfo{}
	info.SetUp()
	info.SqlName = strings.ToUpper(table.Name)
	info.Package = "api"
	info.TableNameLow = strings.ToLower(table.Name)
	info.FirstColumnName = strings.ToLower(columns[0].Name)
	if len(columns) > 1 {
		info.SecondColumnName = strings.ToLower(columns[1].Name)
	} else {
		info.SecondColumnName = strings.ToLower("y" + columns[0].Name)
	}
	info.Fields = fileds
	info.Columns = columns

	createServerFile("cmd/sql/mysql/"+""+strings.ToLower(table.Name)+".base.xsql", "sql.base.y", info)
	createServerFile("cmd/sql/mysql/"+""+strings.ToLower(table.Name)+".read.stpl", "sql.read.y", info)
	createServerFile("cmd/sql/mysql/"+""+strings.ToLower(table.Name)+".count.stpl", "sql.count.y", info)
}

//const BaseTemp = `{{$c := .Columns}}
//-- id: create-{{.TableNameLow}}
//INSERT INTO {{.TableNameLow}} (
//{{with .Columns}}
//	{{range $k, $v := .}}
//	{{Low .Name}}{{IsLast $k $c}}
//	{{end}}
//{{end}}
//) VALUES (
//{{with .Columns}}
//	{{range $k, $v := .}}
//	?{{Low .Name}}{{IsLast $k $c}}
//	{{end}}
//{{end}}
//)
//
//-- id: update-{{.TableNameLow}}
//UPDATE
//  {{.TableNameLow}} set
//  {{with .Columns}}
//  	{{range $k, $v := .}}
//  	{{Low .Name}} = ?{{Low .Name}}{{IsLast $k $c}}
//  	{{end}}
//  {{end}}
//WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}
//
//-- id: read-{{.TableNameLow}}-by-{{.FirstColumnName}}
//SELECT * FROM {{.TableNameLow}} WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}
//
//-- id: delete-{{.TableNameLow}}-by-{{.FirstColumnName}}
//DELETE FROM {{.TableNameLow}} WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}
//
//-- id: update-{{.TableNameLow}}-{{.SecondColumnName}}-by-{{.FirstColumnName}}
//UPDATE
//  {{.TableNameLow}} set
//  	{{.SecondColumnName}} = ?{{.SecondColumnName}}
//WHERE {{.FirstColumnName}} = ?{{.FirstColumnName}}`
//
//const ReadTemp = `SELECT
// {{with .Columns}}
// 	{{range .}}
// 	{{Low .Name}},
// 	{{end}}
// {{end}}
// FROM
// {{.TableNameLow}} where 1=1
//  {{with .Columns}}
//  	{{range .}}
//  	{% if {{Low .Name}} %} and {{Low .Name}} = ?{{Low .Name}} {% endif %}
//  	{{end}}
//  {{end}}
//limit ?offset,?limit
//	`
//const CountTemp = `SELECT
// count(1) count
// FROM
// t_user_info T where 1=1
// {{with .Columns}}
//   	{{range .}}
//   	{% if {{Low .Name}} %} and {{Low .Name}} = ?{{Low .Name}} {% endif %}
//   	{{end}}
//   {{end}}
//`
