package server

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/db"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"strings"
)

type ModelInfo struct {
	//Auth             string
	ControllerName string
	ModelName      string
	//Package          string
	//ProjectUri       string
	//PreUrl           string
	//FirstColumnName  string
	//SecondColumnName string
	//TableNameLow     string
	//TableNameUpper   string
	Fields []Field
	//Columns          []*core.Column
	handler.BaseInfo
}

//const ControllerTemplate = `//{{.Auth}}
//package {{.Package}}
//
//type {{.ControllerName}} scontroller {
//{{with .Fields}}
//	{{range .}}
//	{{.Name}}        {{.Type}}     {{.Comment}}
//	{{end}}
//{{end}}
//}
//`

//type Field scontroller {
//	Name    string
//	Type    string
//	Comment string
//}

var CreateModelHandler handler.HandlerOneTableFunc = func(table *common.Table, columns []*common.Column) {
	beego.Debug("CreateModelHandler")

	fileds := make([]Field, 0)
	fieldCommont := ""
	fieldName := ""
	for _, column := range columns {

		fieldCommont = "`db:\"" + column.Name + "\"` //" + handler.ConvertComment(column.Comment)
		//moduleCommont = "`json:\"" + strings.ToLower(column.Name) + "\"`"
		fieldName = strings.ToUpper(column.Name[:1]) + strings.ToLower(column.Name[1:])
		fieldType, ok := db.DBTYPE[column.SQLType.Name]
		if !ok {
			fieldType = "string"
		}

		fileds = append(fileds, Field{fieldName, fieldType, fieldCommont})
	}

	info := ModelInfo{}
	info.SetUp()
	info.ControllerName = strings.ToUpper(table.Name) + "_CONTROLLER"
	info.ModelName = strings.ToUpper(table.Name) + "_MODEL"
	info.Package = "model"
	info.TableNameLow = strings.ToLower(table.Name)
	info.TableNameUpper = strings.ToUpper(table.Name)
	info.FirstColumnName = columns[0].Name
	if len(columns) > 1 {
		info.SecondColumnName = columns[1].Name
	} else {
		info.SecondColumnName = "y" + columns[0].Name
	}
	info.Fields = fileds
	info.Columns = columns

	createServerFile("model/"+"model_"+strings.ToLower(table.Name)+".go", "model.y", info)
}
