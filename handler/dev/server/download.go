package server

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"os"
)

var DownloadTemplateHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("DownloadTemplateHandler")

	//mask := syscall.Umask(0)
	//defer syscall.Umask(mask)
	filemode := os.ModePerm
	now := common.GetNowTimeYYYYMMDDhhmmss()
	tempdir := cfg.Cfg.GetServerTempPath() + now + "/"
	e := os.MkdirAll(tempdir, filemode)
	if e != nil {
		panic(e)
	}
	savefilepath := tempdir + "server.zip"
	handler.DownloadFile(common.SvrTemplateZipUrl, savefilepath)

	handler.Unzip(savefilepath, tempdir)

	cfg.Cfg.ServerTemplatePath = tempdir + "server/"
}
