package server

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/handler"
	"os"
	"text/template"
	"strings"
)

func createServerFile(filepath string, templatefilename string, info interface{}) {
	//endsWith := strings.HasSuffix("suffix", "fix") // true
	if strings.HasSuffix(filepath, "test.go") {
		filepath = filepath[0:strings.LastIndex(filepath, ".go")] + "_y.go"
	}
	tmpl, err := template.New(templatefilename).Funcs(handler.MyFunc).ParseFiles(cfg.Cfg.ServerTemplatePath + templatefilename)
	if err != nil {
		panic(err)
	}
	file, err := os.Create(cfg.Cfg.GetServerProjcetPath() + filepath)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(file, &info)
	if err != nil {
		panic(err)
	}
}
