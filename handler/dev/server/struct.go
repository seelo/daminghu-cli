package server

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/db"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"strings"
)

type StructInfo struct {
	StructName string
	Fields     []Field
	handler.BaseInfo
}

//const StructTemplate = `//{{.Auth}}
//package {{.Package}}
//
//type {{.StructName}} struct {
//{{with .Fields}}
//	{{range .}}
//	{{.Name}}        {{.Type}}     {{.Comment}}
//	{{end}}
//{{end}}
//}
//`

type Field struct {
	Name    string
	Type    string
	Comment string
}

var CreateStructHandler handler.HandlerOneTableFunc = func(table *common.Table, columns []*common.Column) {

	beego.Debug("CreateStructHandler")

	fileds := make([]Field, 0)
	fieldCommont := ""
	fieldName := ""
	for _, column := range columns {
		//"db:" + "\"" + strings.ToLower(column.Name) + "\"" + ","
		fieldCommont = "`" + "json:" + "\"" + strings.ToLower(column.Name) + "\"" + "` //" + handler.ConvertComment(column.Comment)
		//moduleCommont = "`json:\"" + strings.ToLower(column.Name) + "\"`"
		fieldName = strings.ToUpper(column.Name[:1]) + strings.ToLower(column.Name[1:])
		//beego.Debug(common.DBTYPE)
		//beego.Debug(column.SQLType.Name)
		fieldType, ok := db.DBTYPE[column.SQLType.Name]
		//beego.Debug(fieldType)
		if !ok {
			fieldType = "string"
		}

		fileds = append(fileds, Field{fieldName, fieldType, fieldCommont})
	}

	info := StructInfo{}
	info.SetUp()
	info.StructName = strings.ToUpper(table.Name)
	info.Package = "db"
	info.Fields = fileds

	createServerFile("db/"+"db_"+strings.ToLower(table.Name)+".go", "struct.y", info)
}
