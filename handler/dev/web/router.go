package web

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type RouterInfo struct {
	handler.BaseInfo
}

var CreateRouterHandler handler.HandlerTablesFunc = func(table []*common.Table) {
	beego.Debug("CreateRouterHandler")
	info := RouterInfo{}
	info.SetUp()
	info.Tables = table

	createWebSrcFile("router/index.js", "router.y", info)
}
