package web

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/handler"
	"os"
	"text/template"
)

func createWebFile(filepath string, templatefilename string, info interface{}) {
	tmpl, err := template.New(templatefilename).Funcs(handler.MyFunc).ParseFiles(cfg.Cfg.WebTemplatePath + templatefilename)
	if err != nil {
		panic(err)
	}
	file, err := os.Create(cfg.Cfg.GetWebProjcetPath() + filepath)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(file, &info)
	if err != nil {
		panic(err)
	}
}

func createWebSrcFile(filepath string, templatefilename string, info interface{}) {
	tmpl, err := template.New(templatefilename).Funcs(handler.MyFunc).ParseFiles(cfg.Cfg.WebTemplatePath + templatefilename)
	if err != nil {
		panic(err)
	}
	file, err := os.Create(cfg.Cfg.GetWebProjcetSrcPath() + filepath)
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(file, &info)
	if err != nil {
		panic(err)
	}
}
