package web

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"strings"
)

type PageInfo struct {
	handler.BaseInfo
}

var CreatePageHandler handler.HandlerOneTableFunc = func(table *common.Table, columns []*common.Column) {
	beego.Debug("CreatePageHandler")
	info := PageInfo{}
	info.SetUp()

	info.TableNameLow = strings.ToLower(table.Name)
	info.TableNameUpper = strings.ToUpper(table.Name)
	info.FirstColumnName = columns[0].Name
	if len(columns) > 1 {
		info.SecondColumnName = columns[1].Name
	} else {
		info.SecondColumnName = "y" + columns[0].Name
	}
	//info.Fields = fileds
	info.Columns = columns
	info.Table = table

	createWebSrcFile("pages/"+strings.ToLower(table.Name)+".vue", "page.y", info)
}
