package web

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"os"
)

var CreateDirHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("CreateDirHandler")

	//mask := syscall.Umask(0)
	//defer syscall.Umask(mask)
	filemode := os.ModePerm
	//os.MkdirAll(cfg.Cfg.WebDistPath, filemode)
	os.MkdirAll(cfg.Cfg.GetWebTempPath(), filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath(), filemode)

	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"api", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"cmd", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"cmd/logs", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"cmd/conf", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"cmd/sql/mysql", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"common", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"db", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"filter", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"model", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"msg", filemode)
	//os.MkdirAll(cfg.Cfg.GetWebProjcetPath()+"router", filemode)
}
