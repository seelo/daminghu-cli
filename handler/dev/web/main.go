package web

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type MainInfo struct {
	handler.BaseInfo
}

var CreateMainHandler handler.HandlerTablesFunc = func(table []*common.Table) {
	beego.Debug("CreateMainHandler")
	info := MainInfo{}
	info.SetUp()
	info.Tables = table
	createWebSrcFile("pages/main.vue", "main.y", info)
}
