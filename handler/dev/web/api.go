package web

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type ApiInfo struct {
	handler.BaseInfo
}

var CreateApiHandler handler.HandlerTablesFunc = func(table []*common.Table) {
	beego.Debug("CreateApiHandler")
	info := ApiInfo{}
	info.SetUp()
	info.Package = "api"
	info.Tables = table
	createWebSrcFile("api/api.js", "api.y", info)
}
