package web

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type HomeInfo struct {
	handler.BaseInfo
}

var CreateHomeHandler handler.HandlerTablesFunc = func(table []*common.Table) {
	beego.Debug("CreateHomeHandler")
	info := HomeInfo{}
	info.SetUp()
	info.Tables = table
	createWebSrcFile("pages/home.vue", "home.y", info)
}
