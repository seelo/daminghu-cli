package web

import (
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
)

type EnvInfo struct {
	handler.BaseInfo
}

var CreateEnvHandler handler.HandlerTablesFunc = func(table []*common.Table) {
	beego.Debug("CreateEnvHandler")
	info := EnvInfo{}
	info.SetUp()
	info.Tables = table
	createWebSrcFile("config/env.js", "env.y", info)
}
