package web

import (
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/common"
	"gitee.com/godY/daminghu-cli/handler"
	"github.com/astaxie/beego"
	"os"
)

var DownloadBaseProjectHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("DownloadHandler")

	//mask := syscall.Umask(0)
	//defer syscall.Umask(mask)
	filemode := os.ModePerm
	now := common.GetNowTimeYYYYMMDDhhmmss()
	tempdir := cfg.Cfg.GetWebTempPath() + now + "/"
	e := os.MkdirAll(tempdir, filemode)
	if e != nil {
		panic(e)
	}
	savefilepath := tempdir + "daminghu-web.zip"
	if common.CmdParam.Jsondata == "" {
		handler.DownloadFile(common.WebBasProZipUrl, savefilepath)
	} else {
		handler.DownloadFile(common.WebBasProNoChromeZipUrl, savefilepath)
	}

	handler.Unzip(savefilepath, tempdir)

	if _, err := os.Stat(cfg.Cfg.GetWebProjcetPath()); os.IsExist(err) {
		// path/to/whatever does not exist
		os.Rename(cfg.Cfg.GetWebProjcetPath(), cfg.Cfg.GetWebProjcetPath()+"bak")
	}
	os.Rename(tempdir+"daminghu-web", cfg.Cfg.GetWebProjcetPath())
}

var DownloadTemplateHandler handler.HandlerTablesFunc = func(table []*common.Table) {

	beego.Debug("DownloadTemplateHandler")

	//mask := syscall.Umask(0)
	//defer syscall.Umask(mask)
	filemode := os.ModePerm
	now := common.GetNowTimeYYYYMMDDhhmmss()
	tempdir := cfg.Cfg.GetWebTempPath() + now + "/"
	e := os.MkdirAll(tempdir, filemode)
	if e != nil {
		panic(e)
	}
	savefilepath := tempdir + "web.zip"
	handler.DownloadFile(common.WebTemplateZipUrl, savefilepath)

	handler.Unzip(savefilepath, tempdir)

	cfg.Cfg.WebTemplatePath = tempdir + "web/"
}
