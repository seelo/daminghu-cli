package handler

import (
	"fmt"
	"gitee.com/godY/daminghu-cli/cfg"
	"gitee.com/godY/daminghu-cli/common"
	"github.com/astaxie/beego"
	"os"
)

var DeleteTempHandler HandlerTablesFunc = func(table []*common.Table) {
	beego.Debug("DeleteTempHandler")

	e := os.RemoveAll(cfg.Cfg.GetWebTempPath())
	if e != nil {
		//panic(e)
		fmt.Println(e)
	}
	e = os.RemoveAll(cfg.Cfg.GetServerTempPath())
	if e != nil {
		//panic(e)
		fmt.Println(e)
	}
}
