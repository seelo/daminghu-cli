package demo

import (
	"gitee.com/godY/daminghu-cli/common"
)

type CreateReq struct {
	Tables []*common.Table `json:"tables"`
}

type CreateRes struct {
	BaseResponse
	Body CreateResBody
}

type BaseResponse struct {
	Result  int    `json:"result"`
	OptType int    `json:"opttype"`
	Message string `json:"message"`
	//Body    interface{} `json:"body"`
	Token string `json:"token"`
}

type CreateResBody struct {
	SvrZipUrl string `json:"svr_zip_url"`
	WebZipUrl string `json:"web_zip_url"`
}
