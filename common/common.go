package common

import (
	"strconv"
	"time"
)

const (
	Version = "0.1"

	//Env = "dev"
	Env = "pro"

	DevBaseUrl = "http://localhost:1227/api/"
	//DevEaseWebZipUrl           = "http://localhost:12345/easyweb.zip"
	//DevSvrTemplateZipUrl       = "http://localhost:12345/server.zip"
	//DevWebTemplateZipUrl       = "http://localhost:12345/web.zip"
	//DevWebBasProZipUrl         = "http://localhost:12345/daminghu-web.zip"
	//DevWebBasProNoChromeZipUrl = "http://localhost:12345/nochrome/daminghu-web.zip"
	//DevBaseUrl                 = "http://sd-tlas-dev.uuzz.com:20802/bjtestapi/"
	DevEaseWebZipUrl           = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/easyweb.zip"
	DevSvrTemplateZipUrl       = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/server.zip"
	DevWebTemplateZipUrl       = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/web.zip"
	DevWebBasProZipUrl         = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/daminghu-web.zip"
	DevWebBasProNoChromeZipUrl = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/nochrome/daminghu-web.zip"

	ProBaseUrl                 = "http://sd-tlas-dev.uuzz.com:20802/bjtestapi/"
	ProEaseWebZipUrl           = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/easyweb.zip"
	ProSvrTemplateZipUrl       = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/server.zip"
	ProWebTemplateZipUrl       = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/web.zip"
	ProWebBasProZipUrl         = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/daminghu-web.zip"
	ProWebBasProNoChromeZipUrl = "http://sd-tlas-dev.uuzz.com:20802/ydl/dl/nochrome/daminghu-web.zip"
)

var (
	BaseUrl                 = ProBaseUrl
	EaseWebZipUrl           = ProEaseWebZipUrl
	SvrTemplateZipUrl       = ProSvrTemplateZipUrl
	WebTemplateZipUrl       = ProWebTemplateZipUrl
	WebBasProZipUrl         = ProWebBasProZipUrl
	WebBasProNoChromeZipUrl = ProWebBasProNoChromeZipUrl

	CreateAllUrl    = BaseUrl + "create/all"
	CreateServerUrl = BaseUrl + "create/server"
	CreateWebUrl    = BaseUrl + "create/web"
)

func init() {

	if Env == "dev" {
		BaseUrl = DevBaseUrl
		EaseWebZipUrl = DevEaseWebZipUrl
		SvrTemplateZipUrl = DevSvrTemplateZipUrl
		WebTemplateZipUrl = DevWebTemplateZipUrl
		WebBasProZipUrl = DevWebBasProZipUrl
		WebBasProNoChromeZipUrl = DevWebBasProNoChromeZipUrl

		CreateAllUrl = BaseUrl + "create/all"
		CreateServerUrl = BaseUrl + "create/server"
		CreateWebUrl = BaseUrl + "create/web"
	}

	//logs.Info(CreateAllUrl)
}

type CmdParamStruct struct {
	BuildType string
	CfgPath   string
	Mode      string
	Jsondata  string
	Email     string
}

var (
	CmdParam CmdParamStruct
)

func init() {
	CmdParam = CmdParamStruct{}
}

func GetNowTime() string {
	now := time.Now()
	s := now.Format("2006-01-02 15:04:05")
	return s
}

func GetNowTimeYYYYMMDDhhmmss() string {
	now := time.Now()
	s := now.Format("20060102150405")
	return s
}

func CreateAuthor() string {
	return "Created by daminghu-cli yasenagat# on " + GetNowTime()
}

func UnixNow() string {
	return strconv.Itoa(int(time.Now().Unix()))
}
